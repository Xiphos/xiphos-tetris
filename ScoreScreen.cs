﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;


namespace Tetris
{
    class ScoreScreen
    {
        private int Score;
        private int Lines;
        private string[] Name;
        private int Cursor;
        private int Width, Height;
        private bool ScoreSaved=false;
        private KeyboardState OldState;
        private int BlockSize;
        private int FirstScore, LastScore;
        private string FilePath;
        List<string> Characters = new List<string>
        {
            "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
            "1","2","3","4","5","6","7","8","9","0"
        };
        private Dictionary<string,Button> Buttons = new Dictionary<string, Button>();
        private List<string> Scores = new List<string>();
        private List<Texture2D> Arrows = new List<Texture2D>();
        private SpriteFont Font;

        public ScoreScreen(int score, int lines, Dictionary<string,Button> buttons, SpriteFont font, int width, int height, int blockSize, List<Texture2D> arrows, string filepath)
        {
            Score = score;
            Lines = lines;
            Buttons = buttons;
            Font = font;
            Width = width;
            Height = height;
            BlockSize = blockSize;
            Name = new string[4];
            for (int i = 0; i < Name.Length; i++) Name[i] = Characters[0];
            OldState = Keyboard.GetState();
            
            FirstScore = 1;
            LastScore = 10;
            Arrows = arrows;
            FilePath = filepath;
            LoadScores();
            //Dont let them save a score of 0
            if (Score == 0)
            {
                ScoreSaved = true;
            }
        }

        public bool Update()
        {
            KeyboardState NewState = Keyboard.GetState();

            //Move the 'cursor' if necessary
            if(NewState.IsKeyDown(Keys.Left) && !OldState.IsKeyDown(Keys.Left))
            {
                if(!(Cursor == 0))
                {
                    Cursor--;
                }
            }
            if (NewState.IsKeyDown(Keys.Right) && !OldState.IsKeyDown(Keys.Right))
            {
                if (!(Cursor == Name.Length-1))
                {
                    Cursor++;
                }
            }

            //Up and down changes the character
            if (NewState.IsKeyDown(Keys.Down) && !OldState.IsKeyDown(Keys.Down))
            {
                //Down increases the index

                int cCharacterIndex = Characters.IndexOf(Name[Cursor]);
                cCharacterIndex++;

                //If we are past the end, loop to the beginning
                if(cCharacterIndex >= Characters.Count)
                {
                    cCharacterIndex = 0;
                }

                Name[Cursor] = Characters[cCharacterIndex];
            }
            //Up and down changes the character
            if (NewState.IsKeyDown(Keys.Up) && !OldState.IsKeyDown(Keys.Up))
            {
                //Up decreases the index

                int cCharacterIndex = Characters.IndexOf(Name[Cursor]);
                cCharacterIndex--;

                //If we are past the end, loop to the beginning
                if (cCharacterIndex < 0)
                {
                    cCharacterIndex = Characters.Count - 1;
                }

                Name[Cursor] = Characters[cCharacterIndex];
            }

            //Move the scoreboard
            if(NewState.IsKeyDown(Keys.PageUp) && !OldState.IsKeyDown(Keys.PageUp))
            {
                //Only scroll up if we are in the middle of the list somehwere
                if(FirstScore > 1)
                {
                    FirstScore--;
                    LastScore--;
                }
            }
            if(NewState.IsKeyDown(Keys.PageDown) && !OldState.IsKeyDown(Keys.PageDown))
            {
                if(LastScore < Scores.Count)
                {
                    FirstScore++;
                    LastScore++;
                }
            }

            //Now update the buttons
            foreach(Button b in Buttons.Values)
            {
                b.Update(Mouse.GetState());
            }

            //They saved
            if(Buttons["Save"].SelfState == Button.State.Released)
            {
                //Only save it if they havent yet
                if (!ScoreSaved)
                {
                    SaveScore();
                    ScoreSaved = true;
                }
            }

            //Return to menu
            if(Buttons["Menu"].SelfState == Button.State.Released)
            {
                return true;
            }

            OldState = NewState;
            return false;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw the name menu
            int Ypadding = BlockSize * 2;
            int XPadding = BlockSize * 5-40;
            int ArrowSpread = BlockSize * 1;
            Buttons["Save"].X = Width - XPadding - 20;
            Buttons["Save"].Y = Height - Ypadding - 110;
            Buttons["Save"].UpdateLocation();
            Buttons["Menu"].X = Buttons["Save"].X;
            Buttons["Menu"].Y = Buttons["Save"].Y + Buttons["Save"].Height;
            Buttons["Menu"].UpdateLocation();

            int effectiveWidth = Width - XPadding * 2;
            int cellWidth = effectiveWidth / Name.Length;
            int cellHeight = (int)Font.MeasureString("O").Y;

            for (int i = 0; i < Name.Length; i++)
            {
                string cChar = Name[i];
                Vector2 position = new Vector2();
                position.X = XPadding + (i * cellWidth);
                position.Y = Height - Ypadding - cellHeight;
                spriteBatch.DrawString(Font, cChar, position, Color.White, 0, new Vector2(0), 0.8f, SpriteEffects.None, 0.5f);

                if(i == Cursor)
                {
                    //Draw the arrows
                    Texture2D upArrow = Arrows[0];
                    Texture2D downArrow = Arrows[1];

                    Vector2 upArrowPosition = position + new Vector2(-10, -upArrow.Height + 2);

                    Vector2 downArrowPosition = position + new Vector2(-9, downArrow.Height);

                    spriteBatch.Draw(upArrow, upArrowPosition);
                    spriteBatch.Draw(downArrow, downArrowPosition);
                }
            }

            //Draw the buttons
            foreach (KeyValuePair<string, Button> b in Buttons)
            {
                b.Value.Draw(spriteBatch);
                Color c = Color.White;
                if (b.Value.SelfState == Button.State.Pressed)
                {
                    c = Color.Black;
                }
                spriteBatch.DrawString(Font, b.Key, new Vector2(b.Value.Width / 2, b.Value.Height / 2) + new Vector2(b.Value.X, b.Value.Y), c, 0, Font.MeasureString(b.Key) / 2, 0.35f, SpriteEffects.None, 1.0f);
            }

            //Draw the previous scores
            int minY = Ypadding;
            int maxY = Height - Ypadding - cellHeight - BlockSize;
            int minX = XPadding;
            int maxX = Width - XPadding;
            int effectiveHeightScoreList = maxY - minY;
            int effectiveWidthScoreList = maxX - minX;

            int sizePerEntry = effectiveHeightScoreList / (LastScore - FirstScore + 1);
            for (int i = FirstScore-1; i< LastScore; i++)
            {
                if(i > Scores.Count - 1)
                {
                    break;
                }
                int posInList = i - (FirstScore - 1);
                string score = (FirstScore + posInList).ToString() + ". " + Scores.ElementAt(i).ToString();

                Vector2 position = new Vector2(minX - BlockSize, minY + (posInList * sizePerEntry));
                spriteBatch.DrawString(Font, score, position, Color.White);
            }
        }
        public void SaveScore()
        {
            List<int> scorevalues = new List<int>();

            foreach(string scoreline in Scores)
            {
                scorevalues.Add(Convert.ToInt32(scoreline.Split().Last()));
            }
            
            for(int i = 0; i <= scorevalues.Count; i++)
            {
                if(i == scorevalues.Count)
                {
                    Scores.Add(String.Join("", Name) + "     " + Score);
                    break;
                }
                if(scorevalues[i] < Score)
                {
                    Scores.Insert(i, String.Join("", Name) + "     " + Score);
                    break;
                }
            }

            TextWriter tw = new StreamWriter(FilePath);

            foreach(string s in Scores)
            {
                tw.WriteLine(s);
            }
           
            tw.Close();
        }
        private void LoadScores()
        {
            // ABCD[5spaces]SCORE
            if (!File.Exists(FilePath))
            {
                File.Create(FilePath);
            }
            string[] lines = File.ReadAllLines(FilePath);
            foreach(string score in lines)
            {
                Scores.Add(score);
            }
        }
    }
}
