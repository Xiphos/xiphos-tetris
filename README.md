# Xiphos Tetris

Tetris clone made with C# using the MonoGame framework.
Some demos:  

## Gameplay
[![Gameplay](https://img.youtube.com/vi/J61t_Xwy71U/0.jpg)](https://youtu.be/J61t_Xwy71U)

## Feature Overview
[![Features](https://img.youtube.com/vi/YATq2a_Xrko/0.jpg)](https://youtu.be/YATq2a_Xrko)