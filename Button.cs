﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tetris
{
    class Button
    {
        public enum State
        {
            None,
            Pressed,
            Hover,
            Released
        }
        public int X, Y;
        private string Name;
        private List<Texture2D> Textures;
        private Rectangle Body;
        private Vector2 Location;
        public int Width, Height;
        public State SelfState;


        public Button(string name, List<Texture2D> textures, int buttonX, int buttonY)
        {
            Name = name;
            Textures = textures;
            X = buttonX;
            Y = buttonY;
            Width = Textures[0].Width;
            Height = Textures[0].Height;
            Body = new Rectangle(X, Y, Width, Height);
            Location = new Vector2(X, Y);
        }

        public bool enterButton()
        {
            MouseState ms = Mouse.GetState();
            if (ms.X < X + Width && ms.X > X && ms.Y < Y + Height && ms.Y > Y)
            {
                return true;
            }
            return false;
        }

        public void Update(MouseState mouseState)
        {
            if (Body.Contains(mouseState.X, mouseState.Y))
            {
                //They pressed the button
                if (mouseState.LeftButton == ButtonState.Pressed)
                    SelfState = State.Pressed;

                //They are just hoevering
                else
                {
                    if (SelfState == State.Pressed)
                    {
                        SelfState = State.Released;
                    }
                    else
                    {
                        SelfState = State.Hover;
                    }
                };
                    
            }
            else
            {
                SelfState = State.None;
            }
        }

        public void UpdateLocation()
        {
            this.Location = new Vector2(this.X, this.Y);
            this.Body = new Rectangle(X, Y, Width, Height);

        }

        // Make sure Begin is called on s before you call this function
        public void Draw(SpriteBatch s)
        {
            s.Draw(Textures[(int)SelfState], Location);
        }
    }
}
