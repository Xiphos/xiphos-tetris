﻿using System;

namespace Tetris
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new TetrisGame(10, 20, 40))
                game.Run();
        }
    }
#endif
}
