﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace Tetris
{
    
    class TetrisGame : Game
    {
        //Variables
        #region
        Queue<Tetromino> UpcomingPieces;
        Tetromino ActivePiece;
        Tetromino HoldPiece;
        int[,] Board;
        int GridW, GridH;
        int BlockSize;
        int DropAccumulator=0;
        int SnakeAccumulator = 0;
        int BoardSpeed = 60;
        int difficulty=0;
        int WindowHeight, WindowWidth;
        int Lines, Score;
        bool enteredSnakeScore = false;
        bool snakeGameRunning = false;
        string GameMode;
        public Color[] TileColors;
        Vector2 GamePosition;
        Vector2 FontOriginSS, FontOriginSN, scoreStartPosSS, scoreStartPosSN;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        KeyboardState OldKBState;
        MouseState OldMState;
        Dictionary<string, Texture2D> OptionsMenuTextures = new Dictionary<string, Texture2D>();
        Dictionary<string,Texture2D> Previews = new Dictionary<string, Texture2D>();
        Dictionary<string, Texture2D> MiscTextures = new Dictionary<string, Texture2D>();
        Dictionary<string, Button> MainMenuButtons = new Dictionary<string, Button>();
        Dictionary<string, Button> GameOverButtons = new Dictionary<string, Button>();
        Dictionary<string, Button> ScoreScreenButtons = new Dictionary<string, Button>();
        Dictionary<string, Button> OptionsMenuButtons = new Dictionary<string, Button>();
        bool PlayMusic = true;
        bool ShowShadow = true;
        ScoreScreen ScoreScreen;
        List<Texture2D> DifficultyTints = new List<Texture2D>();
        int TintCount;
        Dictionary<string, SpriteFont> Fonts = new Dictionary<string, SpriteFont>();
        List<Texture2D> BlockTextures = new List<Texture2D>();
        List<Texture2D> Arrows = new List<Texture2D>();
        List<int> JustDestroyed = new List<int>();
        int JustDestroyedAnimCounter;
        bool StartAnimJustDestroyed;
        SnakeGameInfo SnakeGameInfo = new SnakeGameInfo();
        SnakeGame SnakeGame;

        Song MainSong;
        Song GameOverSong;
        Song MenuTheme;
        Song CurrentSong;
        bool startsong = false;
        Dictionary<string,SoundEffect> SoundEffects = new Dictionary<string, SoundEffect>();

        Controls MainControls;
        Controls NewControls;
        bool SettingControl = false;
        string ControlToSet;
        int ControlChangingAccumulator = 0;

        int StartDrawingSavedCounter = 0;
        string SavedNotif;

        bool TrueGravity = false;

        int LineDestroyer = -1;
        int LineDestroyHoldCounter;
        bool Animating = false;

        bool FlashAnimation = false;
        #endregion

        public static string[] TileNames = new string[]
        {
            "I",
            "O",
            "T",
            "J",
            "L",
            "S",
            "Z"
        };

        bool fulldebug = false;

        //Constructor
        public TetrisGame(int gridWidth, int gridHeight, int blockSize)
        {
            this.Window.Title = "Tetris";
            Debug.WriteLine("--Tetris Game Constructed--");
            //Set the board
            Board = new int[gridWidth, gridHeight];
            GridW = gridWidth;
            GridH = gridHeight;
            ResetBoard();

            //Set the controls
            MainControls = new Controls();
            LoadControls();
            NewControls = new Controls();
            NewControls = MainControls;
            

            //Default coloring scheme
            TileColors = new Color[]
            {
                Color.Black,
                new Color(63,224,208),
                new Color(230,230,0),
                new Color(129,0,127),
                new Color(250,166,0),
                new Color(0,0,253),
                new Color(253,0,0),
                new Color(165,43,42)
            };

            //Graphics stuff
            BlockSize = blockSize;
            graphics = new GraphicsDeviceManager(this);
            GamePosition = new Vector2(BlockSize, BlockSize - 30);
            Content.RootDirectory = "Content";

            Random rnd = new Random();
            UpcomingPieces = new Queue<Tetromino>(5);
            //Enqueue 5 random blocks
            for (int i = 0; i < 5; i++)
            {
                UpcomingPieces.Enqueue(GetRandomBlock(rnd.Next(0, 7), 3, -4));
            }

            //Set the default hold tetromino
            HoldPiece = new Tetromino("I", 0, -4);

            //Set the first active piece
            ActivePiece = GetRandomBlock(3, -4);

            //Set the window size
            WindowWidth = (GridW * BlockSize) + (8 * BlockSize);
            WindowHeight = (GridH * BlockSize) + (2 * BlockSize);

            //Set the gamemode
            GameMode = "MAINMENU";
            CurrentSong = MenuTheme;
            startsong = false;

            //Get the keyboard state
            OldKBState = Keyboard.GetState();
        }

        //Overrides
        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = WindowHeight - BlockSize - 20;
            graphics.PreferredBackBufferWidth = WindowWidth;
            graphics.ApplyChanges();

            /*
            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromMilliseconds(msUpdate);
            */
            base.Initialize();
        }
        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (string TetrominoName in TileNames)
            {
                Previews.Add(TetrominoName, Content.Load<Texture2D>("Tiles/Previews/" + TetrominoName));
            }
            MiscTextures.Add("NextLabel", Content.Load<Texture2D>("Next"));
            MiscTextures.Add("GameBackground", Content.Load<Texture2D>("Textures/Background"));

            Texture2D pauseRect = new Texture2D(graphics.GraphicsDevice,WindowWidth, WindowHeight);
            Color[] pauseData = new Color[WindowWidth * WindowHeight];
            for (int i = 0; i < pauseData.Length; i++) pauseData[i] = new Color (10,10,10,100);
            pauseRect.SetData(pauseData);

            MiscTextures.Add("PauseBackground", pauseRect);

            Texture2D GameOverRect = new Texture2D(graphics.GraphicsDevice, WindowWidth, WindowHeight);
            Color[] GameOverData = new Color[WindowWidth * WindowHeight];
            for (int i = 0; i < pauseData.Length; i++) GameOverData[i] = new Color(10, 10, 10, 220);
            GameOverRect.SetData(GameOverData);

            MiscTextures.Add("GameOverBackground", GameOverRect);

            MiscTextures.Add("Cursor", Content.Load<Texture2D>("Cursor"));

            Texture2D LineDestroyedRect = new Texture2D(graphics.GraphicsDevice, BlockSize * GridW, BlockSize);
            Color[]  LineDestroyedRectData = new Color[BlockSize*GridW*BlockSize];
            for (int i = 0; i < LineDestroyedRectData.Length; i++) LineDestroyedRectData[i] = new Color(255, 255, 255);
            LineDestroyedRect.SetData(LineDestroyedRectData);
            MiscTextures.Add("Line Destroyed", LineDestroyedRect);

            Texture2D LineDestroyedRectG = new Texture2D(graphics.GraphicsDevice, BlockSize * GridW, BlockSize);
            Color[] LineDestroyedRectDataG = new Color[BlockSize * GridW * BlockSize];
            for (int i = 0; i < LineDestroyedRectDataG.Length; i++) LineDestroyedRectDataG[i] = new Color(0, 200,0);
            LineDestroyedRectG.SetData(LineDestroyedRectDataG);
            MiscTextures.Add("Tetris", LineDestroyedRectG);

            //Load the font
            Fonts.Add("Pixelfont", Content.Load<SpriteFont>("Fonts/Pixelfont"));

            //Load the arrows
            Arrows.Add(Content.Load<Texture2D>("Arrow_Up"));
            Arrows.Add(Content.Load<Texture2D>("Arrow_Down"));

            //Load buttons
            List<Texture2D> sbTextures = DefaultButton("Start Tetris", spriteBatch);
            List<Texture2D> resetbuttonTextures = DefaultButton("Reset", spriteBatch);
            List<Texture2D> enterscoreTextures = DefaultButton("Enter Score", spriteBatch);
            List<Texture2D> savescoreTextures = DefaultButton("Save Score", spriteBatch);
            List<Texture2D> menuscoreTextures = DefaultButton("Menu", spriteBatch);
            List<Texture2D> goToOptionsTextures = DefaultButton("Options", spriteBatch);

            Button StartButton = new Button("StartButton", sbTextures, (WindowWidth / 2) - (sbTextures[0].Width / 2), (WindowHeight / 2) - (sbTextures[0].Height / 2));
            MainMenuButtons.Add("Start Tetris", StartButton);
            Button OptionsButton = new Button("Options", goToOptionsTextures, StartButton.X, StartButton.Y + StartButton.Height);
            MainMenuButtons.Add("Options", OptionsButton);
            Button ResetButton = new Button("ResetButton", resetbuttonTextures, (WindowWidth / 2) - (sbTextures[0].Width / 2), (WindowHeight / 2) - (sbTextures[0].Height / 2) + 3*BlockSize);
            GameOverButtons.Add("Reset", ResetButton);
            Button EnterScoreButton = new Button("EnterScoreButton", enterscoreTextures, ResetButton.X, ResetButton.Y + BlockSize * 3);
            GameOverButtons.Add("Enter Score", EnterScoreButton);
            Button SaveScoreButton = new Button("SaveScoreButton", savescoreTextures, 0, 0);
            ScoreScreenButtons.Add("Save", SaveScoreButton);
            Button MenuButton = new Button("ReturnToMenu", menuscoreTextures, 0, 0);
            ScoreScreenButtons.Add("Menu", MenuButton);

            //Create block textures
            for(int i = 0; i< 8; i++)
            {
                BlockTextures.Add(Content.Load<Texture2D>("Tiles/"+i.ToString()));
            }
            
            //Load difficulty tints
            List<Color> DifficultyColors = new List<Color>
            {
                Color.Red,
                Color.Green,
                Color.Yellow,
                Color.Purple,
                Color.Blue,
                Color.Turquoise,
                Color.Indigo
            };
            foreach(Color c in DifficultyColors)
            {
                Texture2D rect = new Texture2D(graphics.GraphicsDevice, WindowWidth, WindowHeight);
                Color[] data = new Color[WindowWidth*WindowHeight];
                for (int i = 0; i < data.Length; i++) data[i] = new Color(c, 130);
                rect.SetData(data);

                DifficultyTints.Add(rect);
            }
            TintCount = DifficultyTints.Count;

            //Load options menu content
            #region
            OptionsMenuTextures.Add("Menu", Content.Load<Texture2D>("OptionsMenu/OptionsMenu"));
            OptionsMenuTextures.Add("Cross", Content.Load<Texture2D>("OptionsMenu/Cross"));
            OptionsMenuTextures.Add("Checkmark", Content.Load<Texture2D>("OptionsMenu/Checkmark"));
            OptionsMenuTextures.Add("Mutemusic", Content.Load<Texture2D>("OptionsMenu/Mutemusic"));
            OptionsMenuTextures.Add("Gravity", Content.Load<Texture2D>("OptionsMenu/Gravity"));
            OptionsMenuTextures.Add("NoGravity", Content.Load<Texture2D>("OptionsMenu/Nogravity"));
            OptionsMenuTextures.Add("Music", Content.Load<Texture2D>("OptionsMenu/Music"));

            List<Texture2D> SaveControlsTextures = DefaultButton("Save", spriteBatch);
            List<Texture2D> DefaultControlsTextures = DefaultButton("Defaults", spriteBatch);
            List<Texture2D> ReturnToMenuTextures = DefaultButton("Menu", spriteBatch);

            Button SaveControls = new Button("Save", SaveControlsTextures, 360 - (SaveControlsTextures[0].Width/2), 703 - (SaveControlsTextures[0].Height / 2));
            Button DefaultControls = new Button("Defaults", DefaultControlsTextures, 360 - (SaveControlsTextures[0].Width / 2), 782 - (SaveControlsTextures[0].Height / 2));
            Button ReturnToMenu = new Button("Menu", ReturnToMenuTextures, 618 - (SaveControlsTextures[0].Width / 2), 782 - (SaveControlsTextures[0].Height / 2));

            OptionsMenuButtons.Add("Save", SaveControls);
            OptionsMenuButtons.Add("Defaults", DefaultControls);
            OptionsMenuButtons.Add("Menu", ReturnToMenu);
            #endregion

            //Load snake stuff
            #region

            SnakeGameInfo.Font = Fonts["Pixelfont"];
            SnakeGameInfo.Arrows = new List<Texture2D>();
            SnakeGameInfo.Arrows = Arrows;
            SnakeGameInfo.BlockSize = BlockSize / 2;
            SnakeGameInfo.WindowHeight = WindowWidth/2;
            SnakeGameInfo.WindowWidth = WindowWidth/2;
            SnakeGameInfo.InitialSize = 5;
            SnakeGameInfo.SaveButton = SaveScoreButton;

            Texture2D whiteBlock = new Texture2D(graphics.GraphicsDevice, SnakeGameInfo.BlockSize, SnakeGameInfo.BlockSize);
            Texture2D blackBlock = new Texture2D(graphics.GraphicsDevice, SnakeGameInfo.BlockSize, SnakeGameInfo.BlockSize);
            Texture2D redBlock = new Texture2D(graphics.GraphicsDevice, SnakeGameInfo.BlockSize, SnakeGameInfo.BlockSize);

            Color[] whiteBlockData = new Color[SnakeGameInfo.BlockSize* SnakeGameInfo.BlockSize];
            Color[] blackBlockData = new Color[SnakeGameInfo.BlockSize * SnakeGameInfo.BlockSize];
            Color[] redBlockData = new Color[SnakeGameInfo.BlockSize * SnakeGameInfo.BlockSize];

            for (int i = 0; i < whiteBlockData.Length; i++)
            {
                whiteBlockData[i] = new Color(0, 0, 0);
                blackBlockData[i] = new Color(255, 255, 255);
                redBlockData[i] = new Color(255, 0,0);
            }

            Texture2D GameOverRectSnake = new Texture2D(graphics.GraphicsDevice, SnakeGameInfo.WindowWidth, SnakeGameInfo.WindowHeight);
            Color[] GameOverDataSnake = new Color[SnakeGameInfo.WindowWidth * SnakeGameInfo.WindowHeight];
            for (int i = 0; i < GameOverDataSnake.Length; i++) GameOverDataSnake[i] = new Color(10, 10, 10, 220);
            GameOverRectSnake.SetData(GameOverDataSnake);

            whiteBlock.SetData(whiteBlockData);
            blackBlock.SetData(blackBlockData);
            redBlock.SetData(redBlockData);

            SnakeGameInfo.Textures = new List<Texture2D>();
            SnakeGameInfo.Textures.Add(blackBlock);
            SnakeGameInfo.Textures.Add(whiteBlock);
            SnakeGameInfo.Textures.Add(redBlock);
            SnakeGameInfo.Textures.Add(GameOverRectSnake);
            SnakeGameInfo.Textures.Add(Content.Load<Texture2D>("Textures/Background"));
            #endregion

            //Load music
            MainSong = Content.Load<Song>("Sounds/Korobeneiki");
            GameOverSong = Content.Load<Song>("Sounds/Clock Town");
            MenuTheme = Content.Load<Song>("Sounds/Deku Palace");
            SoundEffects.Add("Tetris", Content.Load<SoundEffect>("Sounds/tetris"));
            SoundEffects.Add("Line Destroyed", Content.Load<SoundEffect>("Sounds/linedestroyed"));
            SoundEffects.Add("Hard Drop", Content.Load<SoundEffect>("Sounds/drop"));
            SoundEffects.Add("Rotate", Content.Load<SoundEffect>("Sounds/rotate"));
            SoundEffects.Add("Stop", Content.Load<SoundEffect>("Sounds/stop"));
            CurrentSong = MenuTheme;
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.75f;

            MiscTextures.Add("Splash", Content.Load<Texture2D>("Splash"));

        }
        protected override void Draw(GameTime gameTime)
        {
            
            //Set the background
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            MouseState ms = Mouse.GetState();

            if (GameMode == "MAINGAME" || GameMode == "PAUSED" || GameMode == "GAMEOVER")
            {
                

                //Draw the background
                spriteBatch.Draw(MiscTextures["GameBackground"], new Vector2(0, 0));
                //Tint based on difficulty
                if(difficulty > 0)
                {
                    spriteBatch.Draw(DifficultyTints[(difficulty - 1) % TintCount], new Vector2(0, 0));
                }

                //Draw the board
                for (int x = 0; x < GridW; x++)
                {
                    for (int y = 0; y < GridH; y++)
                    {
                        //Get the color data
                        int cTile = Board[x, y];

                        //Get the location to draw the box
                        Vector2 pos = new Vector2(x * BlockSize, y * BlockSize);
                        pos += GamePosition;

                        //Draw it (dont draw the pieces if we are paused)
                        if (GameMode == "PAUSED")
                        {
                            spriteBatch.Draw(BlockTextures[0], pos);
                        }
                        else
                        {
                            spriteBatch.Draw(BlockTextures[cTile], pos);
                        }
                    }
                }

                //Draw the active piece
                for (int x = ActivePiece.gxPos; x < ActivePiece.gxPos + 4; x++)
                {
                    for (int y = ActivePiece.gyPos; y < ActivePiece.gyPos + 4; y++)
                    {
                        //Get the color data
                        int cTile = ActivePiece.TileSet[x - ActivePiece.gxPos, y - ActivePiece.gyPos];
                        if (y < 0)
                        {
                            continue;
                        }
                        //Only draw full tiles
                        if (cTile != 0)
                        {
                            //Get the location to draw the box
                            Vector2 pos = new Vector2(x * BlockSize, y * BlockSize);
                            pos += GamePosition;

                            //Draw it
                            spriteBatch.Draw(BlockTextures[cTile], pos);
                        }
                    }
                }

                //Draw the shadow
                if (ShowShadow)
                {
                    Tetromino Shadow = GetShadow();
                    for (int x = Shadow.gxPos; x < Shadow.gxPos + 4; x++)
                    {
                        for (int y = Shadow.gyPos; y < Shadow.gyPos + 4; y++)
                        {
                            //Get the color data
                            int cTile = Shadow.TileSet[x - Shadow.gxPos, y - Shadow.gyPos];
                            if (y < 0)
                            {
                                continue;
                            }
                            //Only draw full tiles
                            if (cTile != 0)
                            {
                                //Get the location to draw the box
                                Vector2 pos = new Vector2(x * BlockSize, y * BlockSize);
                                pos += GamePosition;

                                //Draw it
                                spriteBatch.Draw(BlockTextures[cTile], pos, TileColors[cTile] * 0.25f);
                            }
                        }
                    }
                }

                //Draw the lines destroyed animation
                if (JustDestroyed.Count > 0 && StartAnimJustDestroyed)
                {
                    JustDestroyedAnimCounter = 8;
                    StartAnimJustDestroyed = false;

                    if(JustDestroyed.Count == 4)
                    {
                        SoundEffects["Tetris"].Play();
                    }
                    else
                    {
                        
                        SoundEffects["Line Destroyed"].Play();
                    }
                }
                if (JustDestroyedAnimCounter > 0)
                {
                    foreach (int row in JustDestroyed)
                    {


                        Vector2 pos = new Vector2(0, row * BlockSize);
                        pos += GamePosition;

                        if (FlashAnimation)
                        {
                            if (JustDestroyed.Count == 4)
                            {
                                spriteBatch.Draw(MiscTextures["Tetris"], pos);
                            }
                            else
                            {
                                spriteBatch.Draw(MiscTextures["Line Destroyed"], pos);
                            }
                        }
                    }
                    JustDestroyedAnimCounter--;
                }

                //Draw the upcoming piece
                Vector2 startingPos = new Vector2(13 * BlockSize, 6 * BlockSize);
                for (int x = 0; x < 4; x++)
                {
                    for (int y = 0; y < 4; y++)
                    {
                        Tetromino next = UpcomingPieces.First();
                        int cTile = next.TileSet[x, y];
                        Color tileCol = TileColors[cTile];

                        //Get the preview image
                        Texture2D preview = Previews[next.Type];

                        //Draw it
                        spriteBatch.Draw(preview, startingPos);

                        //Draw the label
                        Texture2D label = MiscTextures["NextLabel"];
                        Vector2 labelOffset = new Vector2(0, -label.Height);
                        spriteBatch.Draw(label, startingPos + labelOffset);
                    }
                }
               
                //Draw the score
                SpriteFont scoreFont = Fonts["Pixelfont"];

                string scoreString = "Score:";
                string scoreNumber = Score.ToString();

                FontOriginSS = scoreFont.MeasureString(scoreString) / 2;
                FontOriginSN = scoreFont.MeasureString(scoreNumber) / 2;

                scoreStartPosSS = startingPos;
                //Middle of preview box
                scoreStartPosSS.X += 2 * BlockSize;
                //Go down to below the box
                scoreStartPosSS.Y += 6 * BlockSize - 20;

                scoreStartPosSN = startingPos;
                //Middle of preview box
                scoreStartPosSN.X += 2 * BlockSize;
                //Go down to below the box
                scoreStartPosSN.Y += 6 * BlockSize + FontOriginSS.Y;


                spriteBatch.DrawString(scoreFont, scoreString, scoreStartPosSS, Color.White, 0, FontOriginSS, 1.0f, SpriteEffects.None, 0.5f);
                spriteBatch.DrawString(scoreFont, scoreNumber, scoreStartPosSN, Color.White, 0, FontOriginSN, 1.0f, SpriteEffects.None, 0.5f);

                //Draw the held piece
                string Hold = "HOLD";

                Vector2 FontOriginHold = scoreFont.MeasureString(Hold) / 2;
                Vector2 holdStartPos = scoreStartPosSN;
                holdStartPos.Y += BlockSize + 20;

                spriteBatch.DrawString(scoreFont, Hold, holdStartPos, Color.White, 0, FontOriginHold, 1.0f, SpriteEffects.None, 0.5f);

                Texture2D heldPreview = Previews[HoldPiece.Type];
                Vector2 HeldTileOrigin = new Vector2(heldPreview.Width/2, heldPreview.Height/2);
                Vector2 HeldTilePos = holdStartPos + new Vector2(0, 3 * BlockSize);
                HeldTilePos -= HeldTileOrigin;

                spriteBatch.Draw(heldPreview, HeldTilePos);

                if(GameMode == "PAUSED")
                {
                    spriteBatch.Draw(MiscTextures["PauseBackground"], new Vector2(0, 0));
                    Vector2 pausedOrigin = scoreFont.MeasureString("PAUSED") / 2;
                    Vector2 pausedStartPos = new Vector2(WindowWidth / 2, WindowHeight / 2);

                    spriteBatch.DrawString(scoreFont, "PAUSED", pausedStartPos, Color.White, 0, pausedOrigin, 1.0f, SpriteEffects.None, 0.5f);
                }
                if(GameMode == "GAMEOVER")
                {
                    spriteBatch.Draw(MiscTextures["GameOverBackground"], new Vector2(0, 0));
                    Vector2 gameoverOrigin = scoreFont.MeasureString("GAME OVER") / 2;
                    Vector2 pausedStartPos = new Vector2(WindowWidth / 2, WindowHeight / 2);

                    spriteBatch.DrawString(scoreFont, "GAME OVER", pausedStartPos, Color.White, 0, gameoverOrigin, 1.0f, SpriteEffects.None, 0.5f);

                    foreach(KeyValuePair<string, Button> b in GameOverButtons)
                    {
                        b.Value.Draw(spriteBatch);
                        Color c = Color.White;
                        if(b.Value.SelfState == Button.State.Pressed)
                        {
                            c = Color.Black;
                        }
                        spriteBatch.DrawString(Fonts["Pixelfont"], b.Key, new Vector2(b.Value.Width/ 2, b.Value.Height / 2) + new Vector2(b.Value.X, b.Value.Y), c, 0, Fonts["Pixelfont"].MeasureString(b.Key) / 2, 0.35f, SpriteEffects.None, 1.0f);
                    }
                }
                
                //Cleanup
            }
            else if(GameMode == "MAINMENU")
            {
                spriteBatch.Draw(MiscTextures["GameBackground"], new Vector2(0, 0));
                spriteBatch.Draw(MiscTextures["Splash"], new Vector2(0, BlockSize));
                foreach (KeyValuePair<string, Button> b in MainMenuButtons)
                {
                    b.Value.Draw(spriteBatch);
                    Color c = Color.White;
                    if (b.Value.SelfState == Button.State.Pressed)
                    {
                        c = Color.Black;
                    }
                    spriteBatch.DrawString(Fonts["Pixelfont"], b.Key, new Vector2(b.Value.Width / 2, b.Value.Height / 2) + new Vector2(b.Value.X, b.Value.Y), c, 0, Fonts["Pixelfont"].MeasureString(b.Key) / 2, 0.4f, SpriteEffects.None, 1.0f);
                }
            }
            else if(GameMode == "SCORESCREEN")
            {
                spriteBatch.Draw(MiscTextures["GameBackground"], new Vector2(0, 0));
                ScoreScreen.Draw(spriteBatch);
            }
            else if(GameMode == "SNAKE")
            {
                SnakeGame.Draw(spriteBatch);
            }
            else if(GameMode == "OPTIONS")
            {
                //Draw background
                spriteBatch.Draw(OptionsMenuTextures["Menu"], new Vector2(0, 0));

                //Draw buttons
                foreach (KeyValuePair<string, Button> b in OptionsMenuButtons)
                {
                    b.Value.Draw(spriteBatch);
                    Color c = Color.White;
                    if (b.Value.SelfState == Button.State.Pressed)
                    {
                        c = Color.Black;
                    }
                    spriteBatch.DrawString(Fonts["Pixelfont"], b.Key, new Vector2(b.Value.Width / 2, b.Value.Height / 2) + new Vector2(b.Value.X, b.Value.Y), c, 0, Fonts["Pixelfont"].MeasureString(b.Key) / 2, 0.4f, SpriteEffects.None, 1.0f);
                }

                //Draw shadow toggle button
                Texture2D ShadowToggle = OptionsMenuTextures["Checkmark"];
                Vector2 OriginST = new Vector2(ShadowToggle.Width / 2, ShadowToggle.Height / 2);
                switch (ShowShadow)
                {
                    case (false):
                        spriteBatch.Draw(OptionsMenuTextures["Cross"], new Vector2(630, 621) -OriginST);
                        break;
                    case (true):
                        spriteBatch.Draw(OptionsMenuTextures["Checkmark"], new Vector2(630, 621) - OriginST);
                        break;
                }

                //Drwa the music toggle button
                Texture2D MusicToggle = OptionsMenuTextures["Music"];
                Vector2 OriginMT = new Vector2(MusicToggle.Width / 2, MusicToggle.Height / 2);
                switch (PlayMusic)
                {
                    case (true):
                        spriteBatch.Draw(OptionsMenuTextures["Music"], new Vector2(64, 778) - OriginMT);
                        break;
                    case (false):
                        spriteBatch.Draw(OptionsMenuTextures["Mutemusic"], new Vector2(64, 778) - OriginMT);
                        break;
                }

                //Draw the gravity toggle button
                Texture2D GravityToggle = OptionsMenuTextures["Gravity"];
                Vector2 OriginGT = new Vector2(GravityToggle.Width / 2, GravityToggle.Height / 2);
                switch (TrueGravity)
                {
                    case (true):
                        spriteBatch.Draw(OptionsMenuTextures["Gravity"], new Vector2(158, 778) - OriginGT);
                        break;
                    case (false):
                        spriteBatch.Draw(OptionsMenuTextures["NoGravity"], new Vector2(158, 778) - OriginGT);
                        break;
                }

                //Draw the controls
                List<Vector2> Centers = new List<Vector2>
                {
                    new Vector2(291,194),
                    new Vector2(291, 302),
                    new Vector2(291, 407),
                    new Vector2(291, 511),
                    new Vector2(291,621),
                    new Vector2(630,194),
                    new Vector2(630, 302),
                    new Vector2(630, 407),
                    new Vector2(630, 511),
                    new Vector2(630,621),
                };

                int i = 0;
                foreach(Keys k in NewControls.KeyCollection)
                {
                    string name = k.ToString();
                    if (name != "None")
                    {
                        Vector2 Origin = Fonts["Pixelfont"].MeasureString(name) / 2;
                        spriteBatch.DrawString(Fonts["Pixelfont"], name, Centers[i], Color.White, 0, Origin, 0.4f, SpriteEffects.None, 0.6f);
                    }
                     i++;
                }

                //Draw the saved notif if need be
                if (StartDrawingSavedCounter > 0)
                {
                    if (SavedNotif == "Saved!")
                    {
                        spriteBatch.DrawString(Fonts["Pixelfont"], SavedNotif, new Vector2(440, 675), Color.Green);
                    }
                    else
                    {
                        spriteBatch.DrawString(Fonts["Pixelfont"], SavedNotif, new Vector2(440, 675), Color.Red);
                    }
                    StartDrawingSavedCounter--;
                }
            }
            //Draw the cursor on top
            if (GameMode != "SNAKE" || SnakeGame.GameState == "SCORESCREEN")
            {
                spriteBatch.Draw(MiscTextures["Cursor"], new Vector2(ms.X, ms.Y));
            }

            //If we enter the snake score screen, change the window size
            if (!enteredSnakeScore && snakeGameRunning)
            {
                if (SnakeGame.GameState == "SCORESCREEN")
                {
                    graphics.PreferredBackBufferWidth = 720;
                    graphics.PreferredBackBufferHeight = 800;
                    graphics.ApplyChanges();
                    enteredSnakeScore = true;
                }
            }
            
            spriteBatch.End();
            base.Draw(gameTime);
        }
        protected override void Update(GameTime gameTime)
        {
            //Exit capability
            MouseState ms = Mouse.GetState();
            KeyboardState newKBState = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
            ButtonState.Pressed)
                Exit();

            if (!startsong && PlayMusic)
            {
                MediaPlayer.Play(CurrentSong);
                startsong = true;
            }

            if (Keyboard.GetState().IsKeyDown(MainControls.MainMenu) && !SettingControl)
            {
                GameMode = "MAINMENU";
                CurrentSong = MenuTheme;
                startsong = false;
                snakeGameRunning = false;
                ResetGame();
                graphics.PreferredBackBufferHeight = WindowHeight;
                graphics.PreferredBackBufferWidth = WindowWidth;
                graphics.ApplyChanges();
                Mouse.WindowHandle = this.Window.Handle;
            }

            
            if (GameMode == "MAINGAME")
            {

                KeyboardState NewKBState = Keyboard.GetState();

                if (NewKBState.IsKeyDown(MainControls.Pause) && !OldKBState.IsKeyDown(MainControls.Pause))
                {
                    GameMode = "PAUSED";
                }

                //Increment the accumulators
                DropAccumulator++;

                //Calculate the right board speed
                //9 difficulties means minimum board speed is 400
                BoardSpeed = 60 - (5 * difficulty);

                //Check for input
                Vector2 velocityNew = new Vector2(0, 1);

                //Check for movements every 4 frames
                if (DropAccumulator != 0 && DropAccumulator % 4 == 0)
                {
                    if (NewKBState.IsKeyDown(MainControls.MoveLeft))
                    {
                        velocityNew.X -= 1;
                    }
                    if (NewKBState.IsKeyDown(MainControls.MoveRight))
                    {
                        velocityNew.X += 1;
                    }
                }

                //Only rotate, drop and hold on keydowns
                if (NewKBState.IsKeyDown(MainControls.RotateClockwise) && !OldKBState.IsKeyDown(MainControls.RotateClockwise))
                {
                    int[,] newTileSet = ActivePiece.Cycle(false);
                    SoundEffects["Rotate"].Play();
                    //Check if the cycle caused a collision, in which case move it a bit
                    List<string> Blockages = new List<string>();
                    for (int x = 0; x < 4; x++)
                    {
                        for (int y = 0; y < 4; y++)
                        {
                            int tile = newTileSet[x, y];


                            if (tile != 0)
                            {
                                int boardX = ActivePiece.gxPos + x;
                                int boardY = ActivePiece.gyPos + y;
                                if (boardX < 0)
                                {
                                    //We rotated into a left frame, whoops!
                                    Blockages.Add("LEFTWALL");
                                }
                                if (boardX >= GridW)
                                {
                                    Blockages.Add("RIGHTWALL");
                                }
                                if (boardY >= GridH)
                                {
                                    Blockages.Add("FLOOR");
                                }
                                if (Blockages.Count > 0 || boardY < 0)
                                {
                                    continue;
                                }
                                int board = Board[ActivePiece.gxPos + x, ActivePiece.gyPos + y];
                                //there is a tile in the way of the rotation
                                if (board > 0)
                                {
                                    Blockages.Add("TILE");
                                }
                            }
                        }
                    }

                    if (Blockages.Count == 0)
                    {
                        ActivePiece.TileSet = newTileSet;
                    }
                    else
                    {
                        if (Blockages.Contains("TILE"))
                        {
                            //A tile blocks the bpath, nothing we can do.
                        }
                        else
                        {
                            //Only these things block the path
                            if (Blockages.Contains("LEFTWALL"))
                            {
                                if (ActivePiece.Type == "S" || ActivePiece.Type == "Z")
                                {
                                    ActivePiece.gxPos++;
                                }
                            }
                            if (Blockages.Contains("RIGHTWALL"))
                            {
                                if (ActivePiece.Type == "S" || ActivePiece.Type == "Z")
                                {

                                    ActivePiece.gxPos--;
                                }
                            }
                            if (Blockages.Contains("FLOOR"))
                            {
                                ActivePiece.gyPos--;
                            }
                        }
                    }
                }
                if (NewKBState.IsKeyDown(MainControls.RotateCounterClockwise) && !OldKBState.IsKeyDown(MainControls.RotateCounterClockwise))
                {
                    int[,] newTileSet = ActivePiece.Cycle(true);
                    SoundEffects["Rotate"].Play();
                    //Check if the cycle caused a collision, in which case move it a bit
                    List<string> Blockages = new List<string>();
                    for (int x = 0; x < 4; x++)
                    {
                        for (int y = 0; y < 4; y++)
                        {
                            int tile = newTileSet[x, y];


                            if (tile != 0)
                            {
                                int boardX = ActivePiece.gxPos + x;
                                int boardY = ActivePiece.gyPos + y;
                                if (boardX < 0)
                                {
                                    //We rotated into a left frame, whoops!
                                    Blockages.Add("LEFTWALL");
                                }
                                if (boardX >= GridW)
                                {
                                    Blockages.Add("RIGHTWALL");
                                }
                                if (boardY >= GridH)
                                {
                                    Blockages.Add("FLOOR");
                                }
                                if (Blockages.Count > 0 || boardY < 0)
                                {
                                    continue;
                                }
                                int board = Board[ActivePiece.gxPos + x, ActivePiece.gyPos + y];
                                //there is a tile in the way of the rotation
                                if (board > 0)
                                {
                                    Blockages.Add("TILE");
                                }
                            }
                        }
                    }

                    if (Blockages.Count == 0)
                    {
                        ActivePiece.TileSet = newTileSet;
                    }
                    else
                    {
                        if (Blockages.Contains("TILE"))
                        {
                            //A tile blocks the bpath, nothing we can do.
                        }
                        else
                        {
                            //Only these things block the path
                            if (Blockages.Contains("LEFTWALL"))
                            {
                                if (ActivePiece.Type == "S" || ActivePiece.Type == "Z")
                                {
                                    ActivePiece.gxPos++;
                                }
                            }
                            if (Blockages.Contains("RIGHTWALL"))
                            {
                                if (ActivePiece.Type == "S" || ActivePiece.Type == "Z")
                                {

                                    ActivePiece.gxPos--;
                                }
                            }
                            if (Blockages.Contains("FLOOR"))
                            {
                                ActivePiece.gyPos--;
                            }
                        }
                    }
                }
                if (NewKBState.IsKeyDown(MainControls.SwapHold) && !OldKBState.IsKeyDown(MainControls.SwapHold))
                {
                    Hold();
                }
                if (NewKBState.IsKeyDown(MainControls.InstantDrop) && !OldKBState.IsKeyDown(MainControls.InstantDrop))
                {
                    //Perform a full drop
                    SoundEffects["Hard Drop"].Play();

                    if (ShowShadow && TrueGravity)
                        Score += 10;
                    if (ShowShadow ^ TrueGravity)
                        Score += 50;
                    else
                        Score += 150;

                    ActivePiece = GetShadow();
                    ImprintTile(ActivePiece);
                    ActivePiece.Falling = false;
                }

                //Just speed up the fall interval
                if (NewKBState.IsKeyDown(MainControls.SpeedDrop))
                {
                    BoardSpeed = 2;
                }

                ActivePiece.Velocity = velocityNew;
                MoveSideways();

                //let the piece fall only if we are on a multiple of the interval time
                if (DropAccumulator != 0 && DropAccumulator % BoardSpeed == 0)
                {
                    // drop
                    DropAccumulator = 0;
                    this.Fall();
                }

                //After falling, the current piece stopped moving, generate a new one
                if (!ActivePiece.Falling)
                {
                    ActivePiece = UpcomingPieces.Dequeue();
                    UpcomingPieces.Enqueue(GetRandomBlock(3, -2));
                }

                //If true gravity, drop pieces
                if (TrueGravity)
                {
                    for (int y = GridH - 2; y > 0; y--)
                    {
                        for (int x = 0; x < GridW; x++)
                        {
                            if (Board[x, y] > 0)
                            {
                                int cBelow = Board[x, y + 1];
                                if (cBelow == 0)
                                {
                                    Board[x, y + 1] = Board[x, y];
                                    Board[x, y] = 0;
                                }
                            }
                        }
                    }
                }

                //Handle destroyed lines
                List<int> destroyed = DestroyedLines();
                if (destroyed.Count > 0)
                {
                    //Save the lines we just destroyed
                    JustDestroyed = destroyed;
                    StartAnimJustDestroyed = true;
                    Animating = true;

                    //Start the animation
                    LineDestroyer = (GridW - 1);
                    LineDestroyHoldCounter = 0;

                    

                    //Calculate the score to add, taken from:
                    //http://tetris.wikia.com/wiki/Scoring\
                    float factor = 1;
                    if (!ShowShadow)
                    {
                        factor = 2;
                    }
                    //Reduce score if playing with true gravity
                    if (TrueGravity)
                    {
                        factor /= 4;
                    }
                    switch (destroyed.Count)
                    {
                        case (1):
                            Score += (int)(20 * (difficulty + 1) * factor);
                            break;
                        case (2):
                            Score += (int)(75 * (difficulty + 1) * factor);
                            break;
                        case (3):
                            Score += (int)(150 * (difficulty + 1) * factor);
                            break;
                        case (4):
                            Score += (int)(600 * (difficulty + 1) * factor);
                            break;
                    }
                    Lines += destroyed.Count;
                    if (Lines % 10 == 0)
                    {
                        difficulty = Math.Min(10, difficulty + 1);
                    }
                }

                //Animation
                if (LineDestroyer > -1)
                {
                    if (LineDestroyHoldCounter % 2 == 0)
                    {
                        foreach (int row in JustDestroyed)
                        {
                            Board[LineDestroyer, row] = 0;
                        }
                        LineDestroyer--;

                        if (LineDestroyer == -1)
                        {
                            Animating = false;
                            if (JustDestroyed.Count == 1)
                            {
                                ShiftDown(JustDestroyed[0] - 1, JustDestroyed[0] + 1);
                                PrintBoard();
                            }
                            else if (JustDestroyed.Count == 2)
                            {
                                if (Math.Abs(JustDestroyed[1] - JustDestroyed[0]) == 1)
                                {
                                    ShiftDown(JustDestroyed[0] - 1, JustDestroyed[1] + 1);
                                    PrintBoard();
                                }
                                else
                                {
                                    ShiftDown(JustDestroyed[0] - 1, JustDestroyed[0] + 1);
                                    PrintBoard();
                                    ShiftDown(JustDestroyed[1] - 1, JustDestroyed[1] + 1);
                                    PrintBoard();
                                }
                            }
                            else if (JustDestroyed.Count == 3)
                            {
                                //3 consecutive numbers
                                if (Math.Abs(JustDestroyed[2] - JustDestroyed[1]) == 1 && Math.Abs(JustDestroyed[1] - JustDestroyed[0]) == 1)
                                {
                                    ShiftDown(JustDestroyed[0] - 1, JustDestroyed[2] + 1);
                                    PrintBoard();
                                }
                                else
                                {
                                    //x0xx
                                    if (Math.Abs(JustDestroyed[1] - JustDestroyed[2]) == 1)
                                    {
                                        ShiftDown(JustDestroyed[0] - 1, JustDestroyed[0] + 1);
                                        PrintBoard();
                                        ShiftDown(JustDestroyed[1] - 1, JustDestroyed[2] + 1);
                                        PrintBoard();
                                    }
                                    // xx0x
                                    else if (Math.Abs(JustDestroyed[1] - JustDestroyed[0]) == 1)
                                    {
                                        ShiftDown(JustDestroyed[0] - 1, JustDestroyed[1] + 1);
                                        PrintBoard();
                                        ShiftDown(JustDestroyed[2] - 1, JustDestroyed[2] + 1);
                                        PrintBoard();
                                    }
                                }
                            }




                            else if (JustDestroyed.Count == 4)
                            {
                                ShiftDown(JustDestroyed[0] - 1, JustDestroyed[3] + 1);
                                PrintBoard();
                            }
                        }
                    }

                    LineDestroyHoldCounter++;
                }



                OldKBState = NewKBState;
            }
            else if (GameMode == "MAINMENU")
            {
                this.Window.Title = "Tetris";
                foreach(Button b in MainMenuButtons.Values)
                {
                    b.Update(ms);
                }
                if(MainMenuButtons["Start Tetris"].SelfState == Button.State.Released)
                {
                    CurrentSong = MainSong;
                    startsong = false;
                    GameMode = "MAINGAME";
                }
                if(MainMenuButtons["Options"].SelfState == Button.State.Released)
                {
                    CurrentSong = MenuTheme;
                    GameMode = "OPTIONS";
                    StartDrawingSavedCounter = 0;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.S) && !OldKBState.IsKeyDown(Keys.S))
                {
                    SnakeGame = new SnakeGame(SnakeGameInfo);
                    graphics.PreferredBackBufferWidth = SnakeGameInfo.WindowWidth;
                    graphics.PreferredBackBufferHeight = SnakeGameInfo.WindowHeight;
                    graphics.ApplyChanges();
                    this.Window.Title = "Snake";
                    GameMode = "SNAKE";
                    CurrentSong = GameOverSong;
                    startsong = false;
                    snakeGameRunning = true;
                    enteredSnakeScore = false;
                }
            }
            else if (GameMode == "GAMEOVER")
            {
                foreach(Button b in GameOverButtons.Values)
                {
                    b.Update(ms);
                }
                if(GameOverButtons["Reset"].SelfState == Button.State.Released)
                {
                    ResetGame();
                    CurrentSong = MainSong;
                    startsong = false;
                    GameMode = "MAINGAME";
                    HoldPiece = new Tetromino("I");
                }
                if(GameOverButtons["Enter Score"].SelfState == Button.State.Released)
                {
                    
                    GameMode = "SCORESCREEN";
                    ScoreScreen = new ScoreScreen(Score, Lines, ScoreScreenButtons, Fonts["Pixelfont"], WindowWidth, WindowHeight, BlockSize, Arrows, "Scores.txt");
                    ResetGame();
                    HoldPiece = new Tetromino("I");
                }
            }
            else if(GameMode == "PAUSED")
            {
                KeyboardState NewKBState = Keyboard.GetState();

                if (NewKBState.IsKeyDown(MainControls.Pause) && !OldKBState.IsKeyDown(MainControls.Pause))
                {
                    CurrentSong = MainSong;
                    startsong = false;
                    GameMode = "MAINGAME";
                }
                OldKBState = NewKBState;
            }
            else if (GameMode == "SCORESCREEN")
            {
                bool returntomenu = false;
                returntomenu = ScoreScreen.Update();
                if (returntomenu)
                {
                    GameMode = "MAINMENU";
                    CurrentSong = MenuTheme;
                    if (PlayMusic)
                    {
                        MediaPlayer.Play(CurrentSong);
                    }
                }
            }
            else if(GameMode == "OPTIONS")
            {
                foreach (Button b in OptionsMenuButtons.Values)
                {
                    b.Update(ms);
                }

                //Check buttons
                if(OptionsMenuButtons["Save"].SelfState == Button.State.Released)
                {
                    //Make sure the new controlsare valid, then save it
                    bool validControls = true;
                    foreach(Keys k in NewControls.KeyCollection)
                    {
                        if(k == Keys.None)
                        {
                            validControls = false;
                        }
                        //Dont let them change the scroll buttons
                        if(k == Keys.PageUp || k == Keys.PageDown)
                        {
                            validControls = false;
                        }
                    }

                    //Start drawing the notification
                    StartDrawingSavedCounter = 150;

                    if (validControls)
                    {
                        MainControls = NewControls;
                        SaveControls();
                        SavedNotif = "Saved!";
                    }
                    else
                    {
                        SavedNotif = "Failed.";
                    }
                }
                if(OptionsMenuButtons["Defaults"].SelfState == Button.State.Released)
                {
                    NewControls.SetDefaultControls();
                }
                if(OptionsMenuButtons["Menu"].SelfState == Button.State.Released)
                {
                    GameMode = "MAINMENU";
                }

                //Check if they toggled the buttons
                if(ms.LeftButton == ButtonState.Released && OldMState.LeftButton == ButtonState.Pressed)
                {
                    int w = 98, h = 59;
                    int l = 242, r = 581;
                    Rectangle toggleMusic = new Rectangle(29, 743, OptionsMenuTextures["Music"].Width, OptionsMenuTextures["Music"].Height);
                    Rectangle toggleShadows = new Rectangle(599,592, OptionsMenuTextures["Checkmark"].Width, OptionsMenuTextures["Checkmark"].Height);
                    Rectangle toggleGravity = new Rectangle(124, 743, OptionsMenuTextures["Gravity"].Width, OptionsMenuTextures["Gravity"].Height);

                    //Left column
                    Rectangle menuButton = new Rectangle(l, 165, w, h);
                    Rectangle rotateCWButton = new Rectangle(l, 273, w, h);
                    Rectangle rotateCCWButton = new Rectangle(l, 378, w, h);
                    Rectangle instantdropButton = new Rectangle(l, 482, w, h);
                    Rectangle speeddropButton = new Rectangle(l, 592, w, h);

                    //Right column
                    Rectangle swapholdButton = new Rectangle(r, 165, w, h);
                    Rectangle pauseButton = new Rectangle(r, 273, w, h);
                    Rectangle moveleftButton = new Rectangle(r, 378, w, h);
                    Rectangle moverightButton = new Rectangle(r, 482, w, h);

                    if (toggleMusic.Contains(ms.Position))
                    {
                        PlayMusic = !PlayMusic;
                        if(PlayMusic == false)
                        {
                            MediaPlayer.Stop();
                        }
                        else
                        {
                            MediaPlayer.Play(CurrentSong);
                        }
                    }
                    if (toggleShadows.Contains(ms.Position))
                    {
                        ShowShadow = !ShowShadow;
                    }
                    if (toggleGravity.Contains(ms.Position))
                    {
                        TrueGravity = !TrueGravity;
                    }

                    //Check if they want to change a key
                    if (!SettingControl)
                    {
                        int i = 0;

                        //Left column
                        if (menuButton.Contains(ms.Position))
                        {
                            NewControls.MainMenu = Keys.None;
                            ControlToSet = "MainMenu";
                        }
                        else if (rotateCWButton.Contains(ms.Position))
                        {
                            NewControls.RotateClockwise = Keys.None;
                            ControlToSet = "RotateClockwise";
                        }
                        else if (rotateCCWButton.Contains(ms.Position))
                        {
                            NewControls.RotateCounterClockwise = Keys.None;
                            ControlToSet = "RotateCounterClockwise";
                        }
                        else if (instantdropButton.Contains(ms.Position))
                        {
                            NewControls.InstantDrop = Keys.None;
                            ControlToSet = "InstantDrop";
                        }
                        else if (speeddropButton.Contains(ms.Position))
                        {
                            NewControls.SpeedDrop = Keys.None;
                            ControlToSet = "SpeedDrop";
                        }

                        //Right column
                        else if (swapholdButton.Contains(ms.Position))
                        {
                            NewControls.SwapHold = Keys.None;
                            ControlToSet = "SwapHold";
                        }
                        else if (pauseButton.Contains(ms.Position))
                        {
                            NewControls.Pause = Keys.None;
                            ControlToSet = "Pause";
                        }
                        else if (moveleftButton.Contains(ms.Position))
                        {
                            NewControls.MoveLeft = Keys.None;
                            ControlToSet = "MoveLeft";
                        }
                        else if (moverightButton.Contains(ms.Position))
                        {
                            NewControls.MoveRight = Keys.None;
                            ControlToSet = "MoveRight";
                        }
                        else
                        {
                            i = 1;
                        }

                        if (i == 0)
                        {
                            SettingControl = true;
                        }
                        NewControls.UpdateCollection();
                    }

                }

                //Update control if need be
                if (SettingControl && ControlChangingAccumulator % 5 == 0 )
                {
                    if (newKBState.GetPressedKeys().Count() > 0)
                    {
                        Keys newControl = newKBState.GetPressedKeys()[0];
                        NewControls.SetControl(ControlToSet, newControl);
                        NewControls.UpdateCollection();
                        SettingControl = false;

                        //Now check for conflicts
                        int i = -1;
                        foreach(Keys k in NewControls.KeyCollection)
                        {
                            i++;
                            //Ignore the one we just set
                            if(NewControls.KeyNames[i].ToString() == ControlToSet)
                            {
                                continue;
                            }

                            //Conflict found, change it to none
                            if(k.ToString() == newControl.ToString())
                            {
                                NewControls.SetControl(NewControls.KeyNames[i].ToString(), Keys.None);
                                SettingControl = true;
                                ControlToSet = NewControls.KeyNames[i];
                                break;
                            }
                        }
                    }
                    ControlChangingAccumulator = 0;                
                }
                ControlChangingAccumulator++;
            }
            else if (GameMode == "SNAKE")
            {
                if (SnakeAccumulator != 0 && SnakeAccumulator % 10 == 0 && SnakeGame.GameState == "MAINGAME")
                {
                    SnakeGame.Move();
                    SnakeAccumulator = 0;
                }
                SnakeGame.Update(Keyboard.GetState());

                if (SnakeGame.GameState == "MAINGAME")
                {
                    SnakeAccumulator++;
                }
            }

            OldMState = ms;
            OldKBState = newKBState;
            base.Update(gameTime);
        }

        //Methods
        #region
        public List<Texture2D> DefaultButton(string s, SpriteBatch spr)
        {
            List<Texture2D> Textures = new List<Texture2D>
            {
            Content.Load<Texture2D>("Buttons/Disabled"),
            Content.Load<Texture2D>("Buttons/Pressed"),
            Content.Load<Texture2D>("Buttons/Active"),
            Content.Load<Texture2D>("Buttons/Active")
        };
            List<RenderTarget2D> targets = new List<RenderTarget2D>();
            foreach(Texture2D t in Textures)
            {
                targets.Add(new RenderTarget2D(t.GraphicsDevice, t.Width, t.Height));
            }

            int i = 0;
            foreach(RenderTarget2D rt in targets)
            {
                Color c = Color.White;
                if(i == 0 || i == 3) { c = Color.Black; };
                GraphicsDevice.SetRenderTarget(rt);

                spriteBatch.Begin();
                spriteBatch.DrawString(Fonts["Pixelfont"], s, new Vector2(rt.Width/2, rt.Height/2), c, 0, Fonts["Pixelfont"].MeasureString(s)/2, 1.0f, SpriteEffects.None, 1.0f);
                spriteBatch.End();
                
                i++;
            }
            GraphicsDevice.SetRenderTarget(null);
            foreach(RenderTarget2D r in targets)
            {
                r.Dispose();
            }
            return Textures;
        }
        public void ShiftDown(int fromThis, int toThis)
        {
            //an error was made, swap them to fix it
            if(fromThis > toThis)
            {
                int temp = fromThis;
                fromThis = toThis;
                toThis = temp;
            }
            
            Debug.WriteLine("Shift Down Occured: " + fromThis.ToString() + " to " + toThis.ToString());
            //Select the top piece
            int[,] topPiece = new int[GridW,fromThis + 1];
            for(int y = 0; y <= fromThis; y++)
            {
                for(int x= 0; x < GridW; x++)
                {
                    topPiece[x, y] = Board[x, y];
                    Board[x, y] = 0;
                }
            }

            //Now put it down there
            int tpY=0;
            for(int y = (toThis-1) - fromThis; y < toThis; y++)
            {             
                for(int x = 0;x < GridW; x++)
                {
                    Board[x, y] = topPiece[x, tpY];
                }
                tpY++;
            }
        }
        public void ResetBoard()
        {
            Debug.WriteLine("Board Reset");
            for(int x = 0; x< GridW; x++)
            {
                for(int y = 0; y < GridH; y++)
                {
                    Board[x, y] = 0;
                }
            }
        }
        public void Fall()
        {
            Debug.WriteLineIf(fulldebug, "Fell");

            //Assign the position after collisions have been checked
            int blockX, blockY;
            blockX = ActivePiece.gxPos;
            blockY = ActivePiece.gyPos;

            int newPosX, newPosY;
            newPosX = ActivePiece.gxPos;
            newPosY = ActivePiece.gyPos;

            //Fall down
            if ((int)ActivePiece.Velocity.Y != 0)
            {
               
                bool clearBelow = true;
                List<int> maxDown = new List<int>();
                int downVelocity = (int)ActivePiece.Velocity.Y;

                for (int tileY = 3; tileY >= 0; tileY--)
                {
                    for (int tileX = 0; tileX < 4; tileX++)
                    {
                        //Tile in the tilesheet
                        int TetrominoTile = ActivePiece.TileSet[tileX, tileY];

                        //We are on an active tile
                        if(TetrominoTile != 0)
                        {
                            //X is fixed
                            int boardX = blockX + tileX;
                            int bottom = blockY + tileY + downVelocity;
                            

                            //If the tile hits the bottom of the board, stop it
                            if(bottom >= GridH)
                            {
                                maxDown.Add(GridH - tileY);
                                clearBelow = false;
                                continue;
                            }
                            if(boardX < 0 || boardX >= GridW)
                            {
                                continue;
                            }

                            //Check for collisions below
                            for (int boardY = blockY + tileY; boardY <= blockY + tileY  + downVelocity; boardY++)
                            {
                                if(boardY < 0 || boardX < 0)
                                {
                                    continue;
                                }
                                if(Board[boardX,boardY] > 0)
                                {
                                    //Max amount you can go down is the position of the previous empty board block (bY -1) minus the
                                    maxDown.Add((boardY-1) - (blockY + tileY));
                                    clearBelow = false;
                                }
                            }
                        }
                        
                    }
                }
                //If its completely clear, go down
                if (clearBelow)
                {
                    newPosY += (int)ActivePiece.Velocity.Y;
                }
                //Otherwise move down as much as possible
                else
                {
                    newPosY += maxDown.Min();
                    //MoveSideways();
                    ImprintTile(ActivePiece);
                    SoundEffects["Stop"].Play();
                    ActivePiece.Falling = false;
                }
            }

            #region
            //Move sideways
            /*
            if(ActivePiece.Velocity.X != 0)
            {
                int sideVelocity = (int)ActivePiece.Velocity.X;
                bool clearRight = true;
                bool clearLeft = true;

                //Move right
                if(sideVelocity > 0)
                {
                    //Iterate from the right
                    for(int boardX = ActivePiece.gxPos + 3; boardX >= ActivePiece.gxPos; boardX--)
                    {
                        //Iterate downwards
                        for(int boardY = ActivePiece.gyPos; boardY < ActivePiece.gyPos + 4; boardY++)
                        {
                            int tileSetX = boardX - ActivePiece.gxPos;
                            int tileSetY = boardY - ActivePiece.gyPos;

                            int cTile = ActivePiece.TileSet[tileSetX, tileSetY];
                            if(boardY < 0)
                            {
                                continue;
                            }
                            //We are on an active tile
                            if(cTile > 0)
                            {
                                //Look at the piece to the right
                                int rightX = boardX + 1;

                                if(rightX >= GridW)
                                {
                                    clearRight = false;
                                    continue;
                                }
                                else
                                {
                                    int rightTile = Board[rightX, boardY];
                                    if(rightTile > 0)
                                    {
                                        clearRight = false;
                                    }
                                }
                            }
                        }
                    }
                    //All clear to the right, move.
                    if (clearRight)
                    {
                        newPosX += (int)ActivePiece.Velocity.X;
                    }
                }

                //Move left
                else if (sideVelocity < 0)
                {
                    //Iterate from the right
                    for (int boardX = ActivePiece.gxPos; boardX < ActivePiece.gxPos + 4; boardX++)
                    {
                        //Iterate downwards
                        for (int boardY = ActivePiece.gyPos; boardY < ActivePiece.gyPos + 4; boardY++)
                        {
                            int tileSetX = boardX - ActivePiece.gxPos;
                            int tileSetY = boardY - ActivePiece.gyPos;

                            int cTile = ActivePiece.TileSet[tileSetX, tileSetY];
                            if(boardY < 0)
                            {
                                continue;
                            }
                            //We are on an active tile
                            if (cTile > 0)
                            {
                                //Look at the piece to the right
                                int leftX = boardX - 1;

                                if (leftX < 0)
                                {
                                    clearLeft = false;
                                    continue;
                                }
                                else
                                {
                                    int leftTile = Board[leftX, boardY];
                                    if (leftTile > 0)
                                    {
                                        clearLeft = false;
                                    }
                                }
                            }
                        }
                    }
                    //All clear to the right, move.
                    if (clearLeft)
                    {
                        newPosX += (int)ActivePiece.Velocity.X;
                    }
                }
            }
            if (ActivePiece.gxPos != 0 && ActivePiece.gyPos != 0)
            {
                for (int boardX = newPosX; boardX < newPosX + 4; boardX++)
                {
                    for (int boardY = newPosY; boardY < newPosY + 4; boardY++)
                    {
                        if (boardX >= GridW || boardY >= GridH || boardX < 0 || boardY < 0)
                        {
                            continue;
                        }
                        if (Board[boardX, boardY] > 0)
                        {
                            newPosX = blockX;
                            ActivePiece.Velocity.X = 0;
                            break;
                        }
                    }
                }
            }
            ActivePiece.gxPos = newPosX;
            */
            #endregion


            ActivePiece.gyPos = newPosY;
        }
        public void MoveSideways()
        {
            //This is what the tetromino will look like after the move
            Tetromino futurePos = new Tetromino(ActivePiece, (int)ActivePiece.Velocity.X + ActivePiece.gxPos, ActivePiece.gyPos);
            bool canMove = true;
            for(int boardX = futurePos.gxPos; boardX <futurePos.gxPos + 4; boardX++)
            {
                for(int boardY = futurePos.gyPos; boardY < futurePos.gyPos + 4; boardY++)
                {
                    int tileX = boardX - futurePos.gxPos;
                    int tileY = boardY - futurePos.gyPos;

                    //Look for conflicts on active tiles
                    if(futurePos.TileSet[tileX,tileY] != 0)
                    {
                        //Check boundaries
                        if(boardX < 0 || boardX >= GridW)
                        {
                            canMove = false;
                            break;
                        }
                        if(boardY >= GridH)
                        {
                            canMove = false;
                            break;
                        }

                        //Dont bother with pre-board pieces
                        if(boardY < 0)
                        {
                            continue;
                        }

                        //Look for tiles
                        if(Board[boardX, boardY] != 0)
                        {
                            canMove = false;
                            break;
                        }
                    }
                }
            }

            //OBSOLETE: Old, clunky collision detection//
            #region
            /*
            if (ActivePiece.Velocity.X != 0)
            {
                int sideVelocity = (int)ActivePiece.Velocity.X;
                bool clearRight = true;
                bool clearLeft = true;

                //Move right
                if(sideVelocity > 0)
                {
                    //Iterate from the right
                    for(int boardX = ActivePiece.gxPos + 3; boardX >= ActivePiece.gxPos; boardX--)
                    {
                        //Iterate downwards
                        for(int boardY = ActivePiece.gyPos; boardY < ActivePiece.gyPos + 4; boardY++)
                        {
                            int tileSetX = boardX - ActivePiece.gxPos;
                            int tileSetY = boardY - ActivePiece.gyPos;

                            int cTile = ActivePiece.TileSet[tileSetX, tileSetY];
                            //We are on an active tile
                            if(cTile > 0)
                            {
                                //Look at the piece to the right
                                int rightX = boardX + 1;


                                //If we are now in the screen yet, we just need to check for walls

                                if(rightX >= GridW)
                                {
                                    clearRight = false;
                                    continue;
                                }
                                if(boardY < 0)
                                {
                                    //we are before the board and the tile to the right isnt a wall, so skip
                                    continue;
                                }
                                if(rightX < 0)
                                {
                                    continue;
                                }
                                else
                                {
                                    int rightTile = Board[rightX, boardY];                      
                                    if(rightTile > 0)
                                    {
                                        clearRight = false;
                                    }
                                }
                            }
                        }
                    }
                    //All clear to the right, move.
                    if (clearRight)
                    {
                        newPosX += (int)ActivePiece.Velocity.X;
                    }
                    else
                    {
                        Debug.WriteLine("COULD NOT MOVE RIGHT");
                    }
                }

                //Move left
                else if (sideVelocity < 0)
                {
                    //Iterate from the right
                    for (int boardX = ActivePiece.gxPos; boardX < ActivePiece.gxPos + 4; boardX++)
                    {
                        //Iterate downwards
                        for (int boardY = ActivePiece.gyPos; boardY < ActivePiece.gyPos + 4; boardY++)
                        {
                            int tileSetX = boardX - ActivePiece.gxPos;
                            int tileSetY = boardY - ActivePiece.gyPos;

                            int cTile = ActivePiece.TileSet[tileSetX, tileSetY];
                            //We are on an active tile
                            if (cTile > 0)
                            {
                                //Look at the piece to the right
                                int leftX = boardX - 1;

                                if (leftX < 0)
                                {
                                    clearLeft = false;
                                    continue;
                                }
                                if(boardY < 0)
                                {
                                    //We are before the board and the left is not the wall, so continue
                                    continue;
                                }
                                else
                                {
                                    int leftTile = Board[leftX, boardY];
                                    if (leftTile > 0)
                                    {
                                        clearLeft = false;
                                    }
                                }
                            }
                        }
                    }
                    //All clear to the right, move.
                    if (clearLeft)
                    {
                        newPosX += (int)ActivePiece.Velocity.X;
                    }
                    else
                    {
                        Debug.WriteLine("COULD NOT MOVE LEFT");
                    }
                }
            }
            if (ActivePiece.gxPos != 0 && ActivePiece.gyPos != 0)
            {
                for (int boardX = newPosX; boardX < newPosX + 4; boardX++)
                {
                    for (int boardY = newPosY; boardY < newPosY + 4; boardY++)
                    {
                        if (boardX >= GridW || boardY >= GridH || boardX < 0 || boardY < 0)
                        {
                            continue;
                        }
                        if (Board[boardX, boardY] > 0)
                        {
                            newPosX = blockX;
                            ActivePiece.Velocity.X = 0;
                            break;
                        }
                    }
                }
            }
            */
            #endregion
           
            if (canMove)
            {
                ActivePiece = futurePos;
            }
            //Debug.WriteLineIf(ActivePiece.Velocity.X !=0, "Sideways move with velocity " + ActivePiece.Velocity.X.ToString());
        }
        public List<int> DestroyedLines()
        {
            //Get all the lines that have been destroyed after the block has been placed
            List<int> lines = new List<int>();
            for(int y = 0; y < GridH; y++)
            {
                bool lineDestroyed = true;
                for(int x= 0; x < GridW; x++)
                {
                    if(Board[x,y] == 0)
                    {
                        lineDestroyed = false;
                    }
                }
                if (lineDestroyed)
                {
                    lines.Add(y);
                }
            }
            Debug.WriteLineIf(lines.Count > 0, lines.Count + " Lines Destroyed: " + string.Join(", ", lines.Select(x => x.ToString()).ToArray()));
            return lines;
        }
        public void ImprintTile(Tetromino block)
        {
            Debug.WriteLine("\'" + block.Type + "\'" + " Tetromino  imprinted at (" + block.gxPos.ToString() + ", " + block.gyPos.ToString() + ")");

            for (int x = block.gxPos; x < block.gxPos + 4; x++)
            {
                for(int y = block.gyPos; y < block.gyPos + 4; y++)
                {
                    int tile = block.TileSet[x - block.gxPos, y - block.gyPos];

                    //If we are imprinting a tile outside of the board then 
                    if(y < 0 && tile != 0)
                    {
                        GameMode = "GAMEOVER";
                        CurrentSong = GameOverSong;
                        startsong = false;
                        continue;
                    }
                    if(y < 0 && tile == 0)
                    {
                        continue;
                    }
                    if(tile != 0 && x < GridW && y < GridH)
                    {
                        Board[x, y] = tile;
                    }
                }
            }
        }
        public Tetromino GetRandomBlock(int xPos, int yPos)
        {
            Random rnd = new Random();
            Tetromino newBlock = new Tetromino(rnd.Next(0, 7), xPos, yPos);
            //Debug.WriteLine("\'" + newBlock.Type + "\'" + " Tetromino created at (" + xPos.ToString() + ", " + yPos.ToString() + ")");

            return newBlock;
        }
        public Tetromino GetRandomBlock(int rnd, int xPos, int yPos)
        { 
            Tetromino newBlock = new Tetromino(rnd, xPos, yPos);
            //Debug.WriteLine("\'" + newBlock.Type + "\'" + " Tetromino created at (" + xPos.ToString() + ", " + yPos.ToString() + ")");
            return newBlock;
        }
        public Color Darken(Color ColorToDarken)
        {

            int darkenFactor = 80;
            int newR = Math.Min((ColorToDarken.R + darkenFactor), 255);
            int newG = Math.Min((ColorToDarken.G + darkenFactor), 255);
            int newB = Math.Min((ColorToDarken.B + darkenFactor), 255);

            return new Color(newR, newG, newB);
        }
        public void ResetGame()
        {
            Debug.WriteLine("Game Reset");

            ResetBoard();
            Random rnd = new Random();
            UpcomingPieces.Clear();
            UpcomingPieces = new Queue<Tetromino>(5);
            //Enqueue 5 random blocks
            for (int i = 0; i < 5; i++)
            {
                UpcomingPieces.Enqueue(GetRandomBlock(rnd.Next(0, 7), 3, -4));
            }

            //Reset score
            Lines = 0;
            Score = 0;

            //Reset difficulty
            difficulty = 0;

            //Set the first active piece
            ActivePiece = GetRandomBlock(3, -2);
        }
        public void Hold()
        {
            if (ActivePiece.gyPos < 4)
            {
                //Swap them
                Tetromino temp = ActivePiece;
                ActivePiece = HoldPiece.RelocatedTo(3, ActivePiece.gyPos);
                HoldPiece = temp;
                Debug.WriteLine("Swap hold success!");
            }
            else
            {
                Debug.WriteLine("Attempted to swap hold, did not work");
            }
        }
        public Tetromino GetShadow()
        {
            bool canGoDown = true;
            Tetromino Shadow = new Tetromino(ActivePiece, ActivePiece.gxPos, ActivePiece.gyPos+1);
            for (int boardX = Shadow.gxPos; boardX < Shadow.gxPos + 4; boardX++)
            {
                for (int boardY = Shadow.gyPos; boardY < Shadow.gyPos + 4; boardY++)
                {
                    int tileX = boardX - Shadow.gxPos;
                    int tileY = boardY - Shadow.gyPos;

                    //Look for conflicts on active tiles
                    if (Shadow.TileSet[tileX, tileY] != 0)
                    {
                        //Check boundaries
                        if (boardX < 0 || boardX >= GridW)
                        {
                            canGoDown = false;
                            break;
                        }
                        if (boardY >= GridH)
                        {
                            canGoDown = false;
                            break;
                        }

                        //Dont bother with pre-board pieces
                        if (boardY < 0)
                        {
                            continue;
                        }

                        //Look for tiles
                        if (Board[boardX, boardY] != 0)
                        {
                            canGoDown = false;
                            break;
                        }
                    }
                }
            }

            //Keep moving the shadow down while you still can
            while (canGoDown)
            {
                Shadow.gyPos++;

                //Check that it can still go down
                for (int boardX = Shadow.gxPos; boardX < Shadow.gxPos + 4; boardX++)
                {
                    for (int boardY = Shadow.gyPos; boardY < Shadow.gyPos + 4; boardY++)
                    {
                        int tileX = boardX - Shadow.gxPos;
                        int tileY = boardY - Shadow.gyPos;

                        //Look for conflicts on active tiles
                        if (Shadow.TileSet[tileX, tileY] != 0)
                        {
                            //Check boundaries
                            if (boardX < 0 || boardX >= GridW)
                            {
                                canGoDown= false;
                                break;
                            }
                            if (boardY >= GridH)
                            {
                                canGoDown = false;
                                break;
                            }

                            //Dont bother with pre-board pieces
                            if (boardY < 0)
                            {
                                continue;
                            }

                            //Look for tiles
                            if (Board[boardX, boardY] != 0)
                            {
                                canGoDown = false;
                                break;
                            }
                        }
                    }
                }
            }
            return new Tetromino(Shadow, Shadow.gxPos, Shadow.gyPos-1);
        }
        public void LoadControls()
        {
            if (!File.Exists("Controls.txt"))
            {
                File.Create("Controls.txt");
            }
            string[] lines = File.ReadAllLines("Controls.txt");
            int i = 0;
            foreach (string control in lines)
            {
                MainControls.SetControl(MainControls.KeyNames[i], (Keys)Enum.Parse(typeof(Keys), control));
                i++;
            }
        }
        public void SaveControls()
        {
            if (!File.Exists("Controls.txt"))
            {
                File.Create("Controls.txt");
            }

            TextWriter tw = new StreamWriter("Controls.txt");

            foreach(Keys k in MainControls.KeyCollection)
            {
                tw.WriteLine(k.ToString());
            }
            tw.Close();
        }
        public void PrintBoard()
        {
            for (int j = 0; j < GridH; j++)
            {
                string srow = j.ToString() + " || ";
                for (int i = 0; i < GridW; i++)
                {
                    srow += Board[i, j].ToString() + " ";
                }
                Debug.WriteLine(srow);
            }
            Debug.WriteLine("***********************");
            Debug.WriteLine("***********************");
        }
        #endregion


    }

    class Tetromino
    {
        #region
        private static int[,] I = new int[,]
{
            {1,1,1,1 },
            {0,0,0,0 },
            {0,0,0,0 },
            {0,0,0,0 }
        };
        private static int[,] IR1 = new int[,]
        {
            {0,1,0,0 },
            {0,1,0,0 },
            {0,1,0,0 },
            {0,1,0,0 }
        };

        private static int[,] O = new int[,]
        {
            {0,0,2,2 }, //x = 0
            {0,0,2,2 }, //x = 1
            {0,0,0,0 }, //x = 2
            {0,0,0,0 } //x = 3
        };


        private static int[,] T = new int[,]
        {
            {0,0,3,0 },
            {0,3,3,0 },
            {0,0,3,0 },
            {0,0,0,0 }
        };
        private static int[,] TR1 = new int[,]
        {
            {0,0,0,0 },
            {0,3,3,3 },
            {0,0,3,0 },
            {0,0,0,0 }
        };
        private static int[,] TR2 = new int[,]
        {
            {0,0,3,0 },
            {0,0,3,3 },
            {0,0,3,0 },
            {0,0,0,0 }
        };
        private static int[,] TR3 = new int[,]
        {
            {0,0,3,0 },
            {0,3,3,3, },
            {0,0,0,0 },
            {0,0,0,0 }
        };
        private static List<int[,]> TMods = new List<int[,]>
        {
            T,
            TR1,
            TR2,
            TR3
        };

        private static int[,] J = new int[,]
        {
            {0,0,0,0 },
            {0,4,0,0 },
            {0,4,0,0 },
            {4,4,0,0 }
        };
        private static int[,] JR1 = new int[,]
        {
            {0,0,0,0 },
            {0,0,0,0 },
            {4,4,4,0 },
            {0,0,4,0 }
        };
        private static int[,] JR2 = new int[,]
        {
            {0,0,0,0 },
            {0,4,4,0 },
            {0,4,0,0 },
            {0,4,0,0 }
        };
        private static int[,] JR3 = new int[,]
        {
            {0,0,0,0 },         
            {4,0,0,0 },
            {4,4,4,0 },
            {0,0,0,0 }
        };
        private static List<int[,]> JMods = new List<int[,]>
        {
            J,
            JR1,
            JR2,
            JR3
        };

        private static int[,] L = new int[,]
        {
            {0,0,0,0 },
            {0,5,0,0 },
            {0,5,0,0 },
            {0,5,5,0 }

        };
        private static int[,] LR1 = new int[,]
        {
            {0,0,0,0 },      
            {0,0,5,0 },
            {5,5,5,0 },
            {0,0,0,0}
        };
        private static int[,] LR2 = new int[,]
        {
            {0,0,0,0 },
            {5,5,0,0},
            {0,5,0,0 },
            {0,5,0,0 }
        };
        private static int[,] LR3 = new int[,]
        {
            {0,0,0,0 },
            {0,0,0,0 },
            {5,5,5,0 },
            {5,0,0,0 },
        };
        private static List<int[,]> LMods = new List<int[,]>
        {
            L,
            LR1,
            LR2,
            LR3
        };

        private static int[,] S = new int[,]
        {
            {0,0,0,0 },
            {0,6,6,0 },
            {6,6,0,0 },
            {0,0,0,0 }
        };
        private static int[,] SR1 = new int[,]
        {
            {0,0,0,0 },
            {6,0,0,0 },
            {6,6,0,0 },
            {0,6,0,0 }
        };
       
        private static int[,] Z = new int[,]
        {
            {0,0,0,0 },       
            {7,7,0,0 },
            {0,7,7,0 },
            {0,0,0,0 }
        };
        private static int[,] ZR1 = new int[,]
        {
            {0,0,0,0 },
            {0,7,0,0 },
            {7,7,0,0 },
            {7,0,0,0 }
        };

        //17 tiles
        private Dictionary<string, int[,]> TileSets = new Dictionary<string, int[,]> {
            ["I"] = I,            
            ["O"] = O,
            ["T"] = T,
            ["J"] = J,
            ["L"] = L,
            ["S"] = S,
            ["Z"] = Z,

            /*
            ["SR1"] = SR1,
            ["LR1"] = LR1,
            ["LR2"] = LR2,
            ["LR3"] = LR3,
            ["JR1"] = JR1,
            ["JR2"] = JR2,
            ["JR3"] = JR3,
            ["ZR1"] = ZR1,
            ["IR1"] = IR1,
            ["TR1"] = TR1,
            ["TR2"] = TR2,
            ["TR3"] = TR3,
            */
        };
        #endregion

        public int[,] Cycle(bool AntiClockwise)
        {
            Debug.WriteLine("Cycled Tetromino");
            int[,] TileSet = new int[4, 4];

            if (!AntiClockwise)
            {
                switch (this.Type)
                {
                    case "I":
                        switch (BlockVersion)
                        {
                            case 0:
                                TileSet = IR1;
                                BlockVersion = 1;
                                break;
                            case 1:
                                TileSet = I;
                                BlockVersion = 0;
                                break;
                        }
                        break;
                    case "O":
                        TileSet = O;
                        break;
                    case "T":
                        if (BlockVersion == TMods.Count - 1)
                        {
                            BlockVersion = 0;
                        }
                        else
                        {
                            BlockVersion++;
                        }
                        TileSet = TMods[BlockVersion];
                        break;
                    case "J":
                        if (BlockVersion == JMods.Count - 1)
                        {
                            BlockVersion = 0;
                        }
                        else
                        {
                            BlockVersion++;
                        }
                        TileSet = JMods[BlockVersion];
                        break;
                    case "L":
                        if (BlockVersion == LMods.Count - 1)
                        {
                            BlockVersion = 0;
                        }
                        else
                        {
                            BlockVersion++;
                        }
                        TileSet = LMods[BlockVersion];
                        break;
                    case "S":
                        switch (BlockVersion)
                        {
                            case 0:
                                TileSet = SR1;
                                BlockVersion = 1;
                                break;
                            case 1:
                                TileSet = S;
                                BlockVersion = 0;
                                break;
                        }
                        break;
                    case "Z":
                        switch (BlockVersion)
                        {
                            case 0:
                                TileSet = ZR1;
                                BlockVersion = 1;
                                break;
                            case 1:
                                TileSet = Z;
                                BlockVersion = 0;
                                break;
                        }
                        break;
                }
            }
            else
            {
                switch (this.Type)
                {
                    case "I":
                        switch (BlockVersion)
                        {
                            case 0:
                                TileSet = IR1;
                                BlockVersion = 1;
                                break;
                            case 1:
                                TileSet = I;
                                BlockVersion = 0;
                                break;
                        }
                        break;
                    case "O":
                        TileSet = O;
                        break;
                    case "T":
                        if (BlockVersion == 0)
                        {
                            BlockVersion = TMods.Count-1;
                        }
                        else
                        {
                            BlockVersion--;
                        }
                        TileSet = TMods[BlockVersion];
                        break;
                    case "J":
                        if (BlockVersion == 0)
                        {
                            BlockVersion = JMods.Count-1;
                        }
                        else
                        {
                            BlockVersion--;
                        }
                        TileSet = JMods[BlockVersion];
                        break;
                    case "L":
                        if (BlockVersion == 0)
                        {
                            BlockVersion = LMods.Count-1;
                        }
                        else
                        {
                            BlockVersion--;
                        }
                        TileSet = LMods[BlockVersion];
                        break;
                    case "S":
                        switch (BlockVersion)
                        {
                            case 0:
                                TileSet = SR1;
                                BlockVersion = 1;
                                break;
                            case 1:
                                TileSet = S;
                                BlockVersion = 0;
                                break;
                        }
                        break;
                    case "Z":
                        switch (BlockVersion)
                        {
                            case 0:
                                TileSet = ZR1;
                                BlockVersion = 1;
                                break;
                            case 1:
                                TileSet = Z;
                                BlockVersion = 0;
                                break;
                        }
                        break;
                }

            }
            return TileSet;
        }

        //Public variables
        public int[,] TileSet = new int[4, 4];
        public int gxPos, gyPos;
        public static string[] TileNames = new string[]
        {
            "I",
            "O",
            "T",
            "J",
            "L",
            "S",
            "Z"
        };
        public static Color[] TileColors = new Color[]
        {
            Color.Black,
            new Color(63,224,208),
            new Color(230,230,0),
            new Color(129,0,127),
            new Color(250,166,0),
            new Color(0,0,253),
            new Color(253,0,0),
            new Color(165,43,42)
        };
        public Vector2 Velocity;
        public bool Falling;
        public string Type;
        public int BlockVersion;

        //Constructors
        public Tetromino(string type)
        {
            //Debug.WriteLine( "\'" + type + "\'" + " Tetromino created");
            TileSet = TileSets[type];
            Velocity = new Vector2(0, 1);
            Falling = true;
            Type = type;
            BlockVersion = 0;
        }
        public Tetromino(Tetromino other, int x, int y)
        {
            this.gxPos = x;
            this.gyPos = y;
            this.Falling = other.Falling;
            this.BlockVersion = other.BlockVersion;
            this.Type = other.Type;
            this.TileSet = other.TileSet;
            this.Velocity = other.Velocity;
        }
        public Tetromino(string type, int x, int y)
        {
            //Debug.WriteLine("\'" + type + "\'" + " Tetromino created at (" + x.ToString() + ", " + y.ToString() + ")");
            gxPos = x;
            gyPos = y;
            TileSet = TileSets[type];
            Velocity = new Vector2(0, 1);
            Falling = true;
            Type = type;
            BlockVersion = 0;
        }
        public Tetromino(int index, int x, int y)
        {
            gxPos = x;
            gyPos = y;
            TileSet = TileSets.ElementAt(index).Value;
            Type = TileSets.ElementAt(index).Key;
            Velocity = new Vector2(0, 1);
            Falling = true;
            BlockVersion = 0;

            //Debug.WriteLine("\'" + Type + "\'" + " Tetromino created at (" + x.ToString() + ", " + y.ToString() + ")");
        }


        //Methods
        public Tetromino RelocatedTo(int x, int y)
        {
            return new Tetromino(this.Type, x, y);
        }
    }

    struct SnakeGameInfo
    {
        public List<Texture2D> Textures;
        public List<Texture2D> Arrows;
        public int BlockSize;
        public int WindowWidth;
        public int WindowHeight;
        public int InitialSize;
        public SpriteFont Font;
        public Button SaveButton;
    }
}
