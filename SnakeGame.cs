﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tetris
{
    class SnakeGame
    {
        private int BlockSize;
        private List<Texture2D> Textures = new List<Texture2D>();
        private List<TailPiece> Snake = new List<TailPiece>();
        private int[,] Board;
        private int GridSize;
        private KeyboardState OldState;
        private int Score;
        private Point Apple;
        private SpriteFont Font;
        private ScoreScreen ScoreScreen;
        private Dictionary<string, Button> SaveScore;
        private List<Texture2D> Arrows;
        private int WindowHeight, WindowWidth;
        public string GameState;
        public SnakeGame(SnakeGameInfo sgi)
        {

            BlockSize = sgi.BlockSize;
            Textures = sgi.Textures;
            GridSize = sgi.WindowWidth / BlockSize;
            WindowHeight = sgi.WindowHeight;
            WindowWidth = sgi.WindowWidth;
            Board = new int[GridSize,GridSize];
            OldState = Keyboard.GetState();
            Font = sgi.Font;

            int x = GridSize / 2;

            for(int y = GridSize/2; y < GridSize/2 + sgi.InitialSize;y++)
            {
                Snake.Add(new TailPiece(new Point(x,y)));
            }
            
            GameState = "MAINGAME";

            SaveScore = new Dictionary<string, Button>();
            SaveScore.Add("Save", sgi.SaveButton);

            Arrows = sgi.Arrows;
            SpawnApple();
        }
        
        public void Update(KeyboardState NewState)
        {
            if (GameState == "MAINGAME")
            {
                //Get the head piece direction
                Vector2 headDirection = new Vector2(0, 0);
                TailPiece.Direction prevHead = Snake[0].GetDirection();
                if (NewState.IsKeyDown(Keys.Up) && !OldState.IsKeyDown(Keys.Up))
                {
                    headDirection.Y -= 1;
                }
                if (NewState.IsKeyDown(Keys.Down) && !OldState.IsKeyDown(Keys.Down))
                {
                    headDirection.Y += 1;
                }
                if (NewState.IsKeyDown(Keys.Left) && !OldState.IsKeyDown(Keys.Left))
                {
                    headDirection.X -= 1;
                }
                if (NewState.IsKeyDown(Keys.Right) && !OldState.IsKeyDown(Keys.Right))
                {
                    headDirection.X += 1;
                }

                //Only set new direction for unique directions
                if (headDirection.Y != 0 ^ headDirection.X != 0)
                {
                    if (headDirection.Y == 1)
                    {
                        Snake[0].SetDirection(TailPiece.Direction.Down);
                        Debug.WriteLine("Started moving Down");
                    }
                    else if (headDirection.Y == -1)
                    {
                        Snake[0].SetDirection(TailPiece.Direction.Up);
                        Debug.WriteLine("Started moving Up");
                    }
                    if (headDirection.X == 1)
                    {
                        Snake[0].SetDirection(TailPiece.Direction.Right);
                        Debug.WriteLine("Started moving Right");
                    }
                    else if (headDirection.X == -1)
                    {
                        Snake[0].SetDirection(TailPiece.Direction.Left);
                        Debug.WriteLine("Started moving Left");
                    }
                }

                //Dont let it move back into itself
                if (Snake[1].Position == Snake[0].nextPos())
                {
                    Snake[0].SetDirection(prevHead);
                }

                //Set the pieces directions
                for (int i = 1; i < Snake.Count; i++)
                {
                    TailPiece nextPiece = Snake[i - 1];
                    TailPiece curPiece = Snake[i];

                    //Get the difference in position (only one component will be different)
                    Vector2 deltaPosition = new Vector2(nextPiece.Position.X - Snake[i].Position.X, nextPiece.Position.Y - Snake[i].Position.Y);
                    if (deltaPosition.X != 0)
                    {
                        //Next piece is to the right
                        if (deltaPosition.X == 1)
                        {
                            Snake[i].SetDirection(TailPiece.Direction.Right);
                        }
                        //Next piece is to the left
                        else
                        {
                            Snake[i].SetDirection(TailPiece.Direction.Left);
                        }
                    }
                    else if (deltaPosition.Y != 0)
                    {
                        if (deltaPosition.Y == 1)
                        {
                            Snake[i].SetDirection(TailPiece.Direction.Down);
                        }
                        else
                        {
                            Snake[i].SetDirection(TailPiece.Direction.Up);
                        }
                    }
                }

                OldState = NewState;
            }
            else if (GameState == "GAMEOVER")
            {
                if(NewState.IsKeyDown(Keys.E) && !OldState.IsKeyDown(Keys.E))
                {
                    GameState = "SCORESCREEN";
                }
                OldState = NewState;
            }
            else if (GameState == "SCORESCREEN")
            {
                ScoreScreen.Update();
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (GameState == "MAINGAME")
            {
                //Draw the board
                for (int x = 0; x < GridSize; x++)
                {
                    for (int y = 0; y < GridSize; y++)
                    {
                        spriteBatch.Draw(Textures[1], new Vector2(x * BlockSize, y * BlockSize));
                    }
                }

                //Draw the snake
                foreach (TailPiece piece in Snake)
                {
                    Vector2 piecePos = new Vector2(piece.Position.X * BlockSize, piece.Position.Y * BlockSize);
                    spriteBatch.Draw(Textures[0], piecePos);
                }

                //Draw the apple
                spriteBatch.Draw(Textures[2], new Vector2(Apple.X * BlockSize, Apple.Y * BlockSize));
            }
            else if(GameState == "GAMEOVER")
            {
                //Draw the board
                for (int x = 0; x < GridSize; x++)
                {
                    for (int y = 0; y < GridSize; y++)
                    {
                        spriteBatch.Draw(Textures[1], new Vector2(x * BlockSize, y * BlockSize));
                    }
                }

                //Draw the snake
                foreach (TailPiece piece in Snake)
                {
                    Vector2 piecePos = new Vector2(piece.Position.X * BlockSize, piece.Position.Y * BlockSize);
                    spriteBatch.Draw(Textures[0], piecePos);
                }

                string gameoverString = "GAME OVER";
                string scoreString = "Score: " + Score.ToString();

                Vector2 gameoverCenter = Font.MeasureString(gameoverString)/2;
                Vector2 scoreStringCenter = Font.MeasureString(scoreString)/2;

                Vector2 centerScreen = new Vector2(WindowWidth / 2, WindowHeight/2);
                Vector2 belowCenter = centerScreen + new Vector2(0, 3 * BlockSize);

                spriteBatch.Draw(Textures[3], new Vector2(0, 0));
                spriteBatch.DrawString(Font, gameoverString, centerScreen, Color.IndianRed, 0, gameoverCenter, 1.0f, SpriteEffects.None, 0.5f);
                spriteBatch.DrawString(Font, scoreString, belowCenter, Color.Purple, 0, scoreStringCenter, 0.5f, SpriteEffects.None, 0.5f);

            }
            else if(GameState == "SCORESCREEN")
            {
                spriteBatch.Draw(Textures[4], new Vector2(0, 0));
                ScoreScreen.Draw(spriteBatch);
            }
        }
        public void Move()
        {
            if (GameState == "MAINGAME")
            {
                //Set the pieces new positions
                for (int i = 0; i < Snake.Count; i++)
                {
                    Snake[i].PreviousPosition = Snake[i].Position;
                    Snake[i].Move();
                };
                if(Snake[0].Position.X >= GridSize || Snake[0].Position.Y >= GridSize || Snake[0].Position.X < 0 || Snake[0].Position.Y < 0)
                {
                    GameState = "GAMEOVER";
                    ScoreScreen = new ScoreScreen(Score, 0, SaveScore, Font, 720, 800, BlockSize, Arrows, "SnakeScores.txt");

                }
                for (int i = 1; i < Snake.Count; i++)
                {
                    if (Snake[0].Position == Snake[i].Position)
                    {
                        GameState = "GAMEOVER";
                        ScoreScreen = new ScoreScreen(Score, 0, SaveScore, Font, 720, 800, BlockSize, Arrows, "SnakeScores.txt");

                    }
                }

                if(Snake[0].Position == Apple)
                {
                    Random rnd = new Random();
                    Score += (100 + rnd.Next(0,50));
                    SpawnApple();
                    ExtendTail();
                }
            }

            for(int x = 0; x < GridSize; x++)
            {
                for(int y = 0; y < GridSize; y++)
                {
                    Board[x, y] = 0;
                }
            }
            foreach(TailPiece tail in Snake)
            {
                if (tail.Position.X >= GridSize || tail.Position.Y >= GridSize || tail.Position.X < 0 || tail.Position.Y < 0)
                {
                    GameState = "GAMEOVER";
                    ScoreScreen = new ScoreScreen(Score, 0, SaveScore, Font, 720, 800, BlockSize, Arrows, "SnakeScores.txt");
                    break;
                }
                Board[tail.Position.X, tail.Position.Y] = 1;
            }

       
        }
        public void SpawnApple()
        {
            Random rnd = new Random();
            int randX = rnd.Next(3, GridSize);
            int randY = rnd.Next(3, GridSize);

            while(Board[randX,randY] == 1)
            {
                //Keep randomixing until we find a good spot
                randX = rnd.Next(0, GridSize);
                randY = rnd.Next(0, GridSize);
            }

            Apple = new Point(randX, randY);
        }
        public void ExtendTail()
        {
            Snake.Add(new TailPiece(Snake.Last().PreviousPosition));
        }
    }

    class TailPiece
    {
        public Point Position;
        public Point PreviousPosition;
        private Direction Velocity;
        public enum Direction
        {
            Up,Right,Left,Down
        }
        public TailPiece(Point position)
        {
            Position = position;
        }
        public void SetDirection(Direction d)
        {
            Velocity = d;
        }
        public Direction GetDirection()
        {
            return Velocity;
        }
        public void Move()
        {
            Direction d = this.Velocity;
            if(d == Direction.Up)
            {
                this.Position.Y--;
            }
            if(d == Direction.Down)
            {
                this.Position.Y++;
            }
            if(d == Direction.Left)
            {
                this.Position.X--;
            }
            if(d== Direction.Right)
            {
                this.Position.X++;
            }
        }
        public Point nextPos()
        {
            TailPiece thisPiece = new TailPiece(this.Position);
            Direction d = this.Velocity;
            if (d == Direction.Up)
            {
                thisPiece.Position.Y--;
            }
            if (d == Direction.Down)
            {
                thisPiece.Position.Y++;
            }
            if (d == Direction.Left)
            {
                thisPiece.Position.X--;
            }
            if (d == Direction.Right)
            {
                thisPiece.Position.X++;
            }
            return thisPiece.Position;
        }
    }
}
