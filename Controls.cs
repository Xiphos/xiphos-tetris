﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Tetris
{
    class Controls
    {
        public Keys MainMenu;
        public Keys RotateClockwise;
        public Keys RotateCounterClockwise;
        public Keys InstantDrop;
        public Keys SpeedDrop;
        public Keys SwapHold;
        public Keys Pause;
        public Keys MoveLeft;
        public Keys MoveRight;
        public List<Keys> KeyCollection;
        public List<string> KeyNames = new List<string>
        {
            "MainMenu",
            "RotateClockwise",
            "RotateCounterClockwise",
            "InstantDrop",
            "SpeedDrop",
            "SwapHold",
            "Pause",
            "MoveLeft",
            "MoveRight"
        };

        public Controls()
        {
            SetDefaultControls();
        }
        public void SetDefaultControls()
        {
            MainMenu = Keys.X;
            RotateClockwise = Keys.Q;
            RotateCounterClockwise = Keys.W;
            InstantDrop = Keys.Up;
            SpeedDrop = Keys.Down;
            SwapHold = Keys.Z;
            Pause = Keys.Escape;
            MoveLeft = Keys.Left;
            MoveRight = Keys.Right;
            UpdateCollection();
        }
        public void SetControl(string control, Keys key)
        {
            switch (control)
            {
                case ("MainMenu"):
                    this.MainMenu = key;
                    break;
                case ("RotateClockwise"):
                    this.RotateClockwise = key;
                    break;
                case ("RotateCounterClockwise"):
                    this.RotateCounterClockwise = key;
                    break;
                case ("InstantDrop"):
                    this.InstantDrop = key;
                    break;
                case ("SpeedDrop"):
                    this.SpeedDrop = key;
                    break;
                case ("SwapHold"):
                    this.SwapHold = key;
                    break;
                case ("Pause"):
                    this.Pause = key;
                    break;
                case ("MoveLeft"):
                    this.MoveLeft = key;
                    break;
                case ("MoveRight"):
                    this.MoveRight = key;
                    break;
                default:
                    break;
            }

            UpdateCollection();
        }
        public void UpdateCollection()
        {
            KeyCollection = new List<Keys>
            {
                MainMenu,
                RotateClockwise,
                RotateCounterClockwise,
                InstantDrop,
                SpeedDrop,
                SwapHold,
                Pause,
                MoveLeft,
                MoveRight,
            };        
        }
    }
}
